<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered_people', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('name',255);
            $table->string('spiritual_name',255)->nullable($value = true);
            $table->string('surname',255);
            $table->char('sex',1);
            $table->integer('age')->unisgned();
            $table->string('email',255);
            $table->date('birthday')->nullable($value = true);
            $table->string('birthplace', 255)->nullable($value = true);
            $table->string('nationality', 255);
            $table->string('address',255)->nullable($value = true);
            $table->string('zip',50)->nullable($value = true);;
            $table->string('city',255);
            $table->string('province','50');
            $table->string('nation',255)->nullable($value = true);
            $table->string('telephone',255);
            $table->string('mobile',255);
            $table->datetime('datetime_arrive')->nullable($value = true);
            $table->datetime('datetime_departure')->nullable($value = true);
            $table->tinyInteger('pets',0)->nullable($value = true);
            $table->text('ser_other_params')->nullable($value = true);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registered_people');
    }
}
