<?php

namespace App\Http\Controllers\Event;

use Illuminate\Http\Request;
use App;
use App\Http\Controllers\Controller;
use App\Event;
use App\RegisteredPeople;
use App\Mail\SendMailNotificationToUser;
use App\Mail\SendMailNotificationToAdmin;
use Mail;
use Illuminate\Support\Carbon;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = 0, Request $request)
    {
		$display = !empty($request->input('preview')) && $request->input('preview') == 'on';//Preview from admin
        $event = null;
		if(!empty($request->input('locale'))){
			App::setLocale($request->input('locale'));
		}
		$locale = App::getLocale();
        $page_title = 'Eventi - Amma Italia';
        $page_description = '';
        $page_motto = '';
        $page_image = '';
        if(!empty($id)){
            $event = \App\Event::find($id);
            $page_title = $event->name;
            $page_description = $event->description;
            $page_motto = $event->motto;
            $page_image = $event->image;
			$page_ser_other_params = !empty($event->ser_other_params) ? unserialize($event->ser_other_params) : '';
            if($event->status == 1 || $display){
                $form_event_fields = unserialize($event->form_event_templates->ser_params);
				
				if(!empty($page_ser_other_params['settings'])){
					$form_event_fields_pre = [];
					$k = 1;
					foreach($page_ser_other_params['settings']['events'] as $additional_event){
						if(!empty($additional_event['description'])){
							if(!empty($additional_event['start']) && !Carbon::parse($additional_event['start'])->isPast()){
								$form_event_fields_pre["event_$k"]['type'] = 'radio';
								$form_event_fields_pre["event_$k"]['it_label'] = '<span style="color:red"><u>'.$additional_event['description'].'</u>';
								$form_event_fields_pre["event_$k"]['en_label'] = '<span style="color:red"><u>'.$additional_event['description'].'</u>';
								if(!empty($additional_event['alert'])){
									$form_event_fields_pre["event_$k"]['it_label'] .= '&nbsp;&nbsp;<span class="blink_me"><strong><u>'.$additional_event['alert'].'</u></strong></span>&nbsp;</span>'; 
									$form_event_fields_pre["event_$k"]['en_label'] .= '&nbsp;&nbsp;<span class="blink_me"><strong><u>'.$additional_event['alert'].'</u></strong></span>&nbsp;</span>'; 
								} else {
									$form_event_fields_pre["event_$k"]['it_label'] .= '</span>';
									$form_event_fields_pre["event_$k"]['en_label'] .= '</span>';
								}
								$form_event_fields_pre["event_$k"]['values'] = ['No' => 0, 'Si' => 1];
								$form_event_fields_pre["event_$k"]['mandatory'] = 1;
								$form_event_fields_pre["event_$k"]['onClick'] = "chooseonlythis(this)";
							}
						}
						$k++;
					}
					$form_event_fields = array_merge($form_event_fields_pre, $form_event_fields);
					if(!empty($page_ser_other_params['settings']['alert_sleeping'])){
						$arr_alert_accommodation['row_alert_accommodation']['type'] = 'row';
						$arr_alert_accommodation['row_alert_accommodation']['it_label'] = '<span class="blink_me" style="color:red">'.$page_ser_other_params['settings']['alert_sleeping'].'</span>';
						//Insert alert before accommodation key
						$pos =  array_search('accommodation', array_keys($form_event_fields));
						$form_event_fields = array_merge(array_slice($form_event_fields, 0, $pos), $arr_alert_accommodation, array_slice($form_event_fields, $pos));
					}
					if(!empty($page_ser_other_params['settings']['hide_sleeping'])){
						unset($form_event_fields['row_alert_accommodation']);
						unset($form_event_fields['accommodation']);
						unset($form_event_fields['linen']);
					}

					//echo '<pre>';
					//print_r($page_ser_other_params['settings']);
					//print_r($form_event_fields_pre);
					//print_r($form_event_fields);
					//echo '</pre>'; 
					//exit;
				}
				
				foreach($form_event_fields as $k => $v){
					if(!empty($v[$locale.'_no_display'])){//Not to display
						$form_event_fields[$k]['hide'] = true;
					}
				}
                return view('event.index', compact('id', 'page_title', 'page_description', 'page_image', 'page_motto', 'event', 'page_ser_other_params', 'form_event_fields', 'locale', 'display'));
            }
        }
        return view('event.event_close', compact('page_title', 'page_description', 'page_image', 'page_motto'));
    }

    public function save($id = 0, Request $request)
    {
        if(!empty($id)){
            $event = Event::find($id); 
			$this->validate_form($event->form_event_template_id, $request);
			return $this->save_form($event, $request);
	    }
    }

	//Validatiion for form template
	public function validate_form($id_form_event_template = null, Request $request)
	{	
		if(!empty($id_form_event_template)){
			if(!empty($request->input('locale'))){
				App::setLocale($request->input('locale'));
			}
			$locale = App::getLocale();
			switch($id_form_event_template){
				default:
					 // Validation params and checks
					//-->Check for block_seva at least one must be selected
					if (
							empty($request->input("block_seva_meal_distribution")) && 
							empty($request->input("block_seva_meal_preparation")) &&
							empty($request->input("block_seva_dish_washing")) &&
							empty($request->input("block_seva_common_cleanup")) &&
							empty($request->input("block_seva_ext_cleanup")) &&
							empty($request->input("block_seva_parking"))
					){
						$request->merge(Array('block_seva' => null));
					} else {
						$request->merge(Array('block_seva' => 1));
					}
					//<--Check for block_seva at least one must be selected
					
					$phoneValidate = "/(^([0-9+-]+)$)/u";
					$arr_error_messages = Array(); 
					$arr_it_error_messages = Array(
												'name.required' => 'Attenzione! il Nome è obbligatorio.',
												'surname.required' => 'Attenzione! il Cognome è obbligatorio.',
												'sex.required' => 'Attenzione! il campo Sesso è obbligatorio.',
												'birthday.required' => 'Attenzione! il campo Data di nascita è obbligatorio.',
												'birthplace.required' => 'Attenzione! il campo Luogo di nascita &egrave; obbligatorio.',
												'nationality.required' => 'Attenzione! il campo Nazionalit&agrave; è obbligatorio.',
												'city.required' => 'Attenzione! il campo Città è obbligatorio.',
												'province.required' => 'Attenzione! il campo Provincia è obbligatorio.',
												'zip.required' => 'Attenzione! il campo CAP è obbligatorio.',
												'zip.digits' => 'Attenzione! il campo CAP deve avere 5 numeri.',
												'email.email' => 'Attenzione il campo email non è valido (solo alfanumerici, @ e punti. No caratteri speciali)',
												'email.required' => 'Attenzione! il campo Email è obbligatorio oppure non &grave; valido.',
												'mobile.required' => 'Attenzione! il campo Cellulare è obbligatorio',
												'mobile.regex' => 'Attenzione! il campo Cellulare non è valido (caratteri ammessi da 0 a 9, +, -).',
												//'telephone.required' => 'Attenzione! il campo Telefono è obbligatorio',
												'telephone.regex' => 'Attenzione! il campo Telefono non è valido (caratteri ammessi da 0 a 9, +, -).',
												'dinner_friday.required' => 'Attenzione! Selezionare una scelta per la Cena del Venerdì',
												'meals_arrive_week.required' => 'Attenzione! Selezionare una scelta per il giorno di arrivo',
												'meals_departure_week.required' => 'Attenzione! Selezionare una scelta per il giorno di partenza',
												'meals_arrive_time.required' => 'Attenzione! Selezionare una scelta per l\ora di arrivo',
												'meals_departure_time.required' => 'Attenzione! Selezionare una scelta per l\'ora di partenza',
												'block_seva.required' => 'Attenzione! Selezionare almeno una scelta per il SEVA SERVIZIO DISINTERESSATO DURANTE IL RITIRO RESIDENZIALE',
												//'block_seva_breakfast_distribution.required' => 'Attenzione! Selezionare una scelta per il Seva preparazione e distribuzione di una colazione',
												//'block_seva_preparation.required' => 'Attenzione! Selezionare una scelta per il Seva preparazione di un pranzo o di una cena',
												//'block_seva_food_pot_washing.required' => 'Attenzione! Selezionare una scelta per il Seva distribuzione pasto e lavaggio pentole',
												//'block_seva_wash_dishes_dining_cleanup.required' => 'Attenzione! Selezionare una scelta per il Seva assistenza lavaggio piatti e riassetto sala da pranzo',
												//'block_seva_cleanup.required' => 'Attenzione! Selezionare una scelta per il Seva pulizia parti comuni',
												'privacy.required' => 'Attenzione! il campo Privacy è obbligatorio.',
					);
					$arr_error_messages = $arr_it_error_messages;
					$validate =  $request->validate([
						'name' => 'required',
						'surname' => 'required',
						'sex' => 'required|string|max:1',
						'birthdate' => 'required|date',
						'birthplace' => 'required',
						'nationality' => 'required',
						'city' => 'required',
						'province' => 'required',
						'zip' => 'required|digits:5',
						'email' => 'required|email',
						'telephone' => 'nullable|string|regex:'.$phoneValidate,
						'mobile' => 'required|string|regex:'.$phoneValidate,
						'meals_arrive_week' => 'required',
						'meals_departure_week' => 'required',
						'meals_arrive_time' => 'required',
						'meals_departure_time' => 'required',
						'block_seva' => 'required',
						//'block_seva_breakfast_distribution' => 'required',
						//'block_seva_lunch_dinner_preparation' => 'required',
						//'block_seva_food_pot_washing' => 'required',
						//'block_seva_wash_dishes_dining_cleanup' => 'required',
						//'block_seva_cleanup' => 'required',
						'privacy' => 'required',
						],
						$arr_error_messages    
					);
				break;
				case 2:
					// Validation params and checks
					//-->Check for block_seva at least one must be selected
					/*
					if (
							empty($request->input("block_seva_meal_distribution")) && 
							empty($request->input("block_seva_vegetable_preparation")) &&
							empty($request->input("block_seva_dish_washing")) &&
							empty($request->input("block_seva_common_cleanup")) &&
							empty($request->input("block_seva_ext_cleanup")) && 
							empty($request->input("block_seva_other")) 
							//empty($request->input("block_seva_retire"))
					){
						$request->merge(Array('block_seva' => null));
					} else {
						$request->merge(Array('block_seva' => 1));
					}
					 */
					//<--Check for block_seva at least one must be selected
					
					$phoneValidate = "/(^([0-9+-]+)$)/u";
					$arr_error_messages = Array(); 
					
					$arr_pre_it_error_messages = [];
					if(null !== $request->input("event_1")){
						$arr_pre_it_error_messages['event_1.required'] = 'Attenzione! è obbligatorio scegliere se si parteciperà a questo evento.';
					}
					if(null != $request->input("event_2")){
						$arr_pre_it_error_messages['event_2.required'] = 'Attenzione! è obbligatorio scegliere se si parteciperà a questo evento.';
					}
					if(null !== $request->input("event_3")){
						$arr_pre_it_error_messages['event_3.required'] = 'Attenzione! è obbligatorio scegliere se si parteciperà a questo evento.';
					}
					if(null !== $request->input("event_4")){
						$arr_pre_it_error_messages['event_4.required'] = 'Attenzione! è obbligatorio scegliere se si parteciperà a questo evento.';
					}
					if(null != $request->input("event_5")){
						$arr_pre_it_error_messages['event_5.required'] = 'Attenzione! è obbligatorio scegliere se si parteciperà a questo evento.';
					}

					$arr_it_error_messages = Array(
												'name.required' => 'Attenzione! il Nome è obbligatorio.',
												'surname.required' => 'Attenzione! il Cognome è obbligatorio.',
												'sex.required' => 'Attenzione! il campo Sesso è obbligatorio.',
												'age.required' => 'Attenzione! il campo Et&agrave; è obbligatorio.',
												//'birthday.required' => 'Attenzione! il campo Data di nascita è obbligatorio.',
												//'birthplace.required' => 'Attenzione! il campo Luogo di nascita &egrave; obbligatorio.',
												'nationality.required' => 'Attenzione! il campo Nazionalit&agrave; è obbligatorio.',
												'city.required' => 'Attenzione! il campo Città è obbligatorio.',
												'province.required' => 'Attenzione! il campo Provincia è obbligatorio.',
												//'zip.required' => 'Attenzione! il campo CAP è obbligatorio.',
												'zip.digits' => 'Attenzione! il campo CAP deve avere 5 numeri.',
												'email.email' => 'Attenzione il campo email non è valido (solo alfanumerici, @ e punti. No caratteri speciali)',
												'email.required' => 'Attenzione! il campo Email è obbligatorio oppure non &grave; valido.',
												'mobile.required' => 'Attenzione! il campo Cellulare è obbligatorio',
												'mobile.regex' => 'Attenzione! il campo Cellulare non è valido (caratteri ammessi da 0 a 9, +, -).',
												'telephone.regex' => 'Attenzione! il campo Telefono non è valido (caratteri ammessi da 0 a 9, +, -).',
												'dinner_friday.required' => 'Attenzione! Selezionare una scelta per la Cena del Venerdì',
												'meals_arrive_week.required' => 'Attenzione! Selezionare una scelta per il giorno di arrivo',
												'meals_departure_week.required' => 'Attenzione! Selezionare una scelta per il giorno di partenza',
												'meals_arrive_time.required' => 'Attenzione! Selezionare una scelta per l\ora di arrivo',
												'meals_departure_time.required' => 'Attenzione! Selezionare una scelta per l\'ora di partenza',
												//'block_seva.required' => 'Attenzione! Selezionare almeno una scelta per il SEVA SERVIZIO DISINTERESSATO DURANTE IL RITIRO RESIDENZIALE',
												'privacy.required' => 'Attenzione! il campo Privacy è obbligatorio.',
					);
					$arr_it_error_messages = array_merge($arr_pre_it_error_messages, $arr_it_error_messages);
					
					$arr_error_messages = $arr_it_error_messages;
					$validate =  $request->validate([
						'name' => 'required',
						'surname' => 'required',
						'sex' => 'required|string|max:1',
						'age' => 'required|integer',
						//'birthdate' => 'required|date',
						//'birthplace' => 'required',
						'nationality' => 'required',
						'city' => 'required',
						'province' => 'required',
						//'zip' => 'required|digits:5',
						'email' => 'required|email',
						'telephone' => 'nullable|string|regex:'.$phoneValidate,
						'mobile' => 'required|string|regex:'.$phoneValidate,
						'meals_arrive_week' => 'required',
						'meals_departure_week' => 'required',
						'meals_arrive_time' => 'required',
						'meals_departure_time' => 'required',
						//'block_seva' => 'required',
						'privacy' => 'required',
						],
						$arr_error_messages    
					);
				break;
			}
		}

	}	
	//save Data
	public function save_form(Event $event, Request $request)
	{
		if(!empty($event->id)){
			$locale = (!empty($request->input('locale')) ? $request->input('locale') : 'it');
			$title = '';
			switch($event->form_event_template_id){
				default:
					$rpeople = new RegisteredPeople();
					$rpeople->event_id = $event->id;
					$rpeople->name = $request->input('name');
					$rpeople->spiritual_name = $request->input('spiritual_name');
					$rpeople->surname = $request->input('surname');
					$rpeople->sex = $request->input('sex');
					$rpeople->birthday = implode("-", array_reverse(explode("-", $request->input('birthdate'))));;
					$rpeople->birthplace = $request->input('birthplace');
					$rpeople->nationality = $request->input('nationality');
					$rpeople->zip = $request->input('zip');
					$rpeople->city = $request->input('city');
					$rpeople->province = $request->input('province');
					$rpeople->email = $request->input('email');
					$rpeople->mobile = $request->input('mobile');
					$rpeople->telephone = !empty($request->input('telephone')) ? $request->input('telephone') : '';
					$other_params = Array();
					$other_params['meals_arrive_week'] = $request->input('meals_arrive_week');
					$other_params['meals_departure_week'] = $request->input('meals_departure_week');
					$other_params['meals_arrive_time'] = $request->input('meals_arrive_time');
					$other_params['meals_departure_time'] = $request->input('meals_departure_time');
					$other_params['meals_allergies'] = $request->input('meals_allergies');
					$other_params['accommodation'] = $request->input('accommodation');
					$other_params['linen'] = $request->input('linen');
					$other_params['block_seva_meal_distribution'] = $request->input('block_seva_meal_distribution');
					$other_params['block_seva_meal_preparation'] = $request->input('block_seva_meal_preparation');
					$other_params['block_seva_dish_washing'] = $request->input('block_seva_dish_washing');
					$other_params['block_seva_common_cleanup'] = $request->input('block_seva_common_cleanup');
					$other_params['block_seva_ext_cleanup'] = $request->input('block_seva_ext_cleanup');
					$other_params['block_seva_parking'] = $request->input('block_seva_parking');
					$other_params['shuttle'] = $request->input('shuttle');
					$other_params['car_pooling'] = $request->input('car_pooling');
					$other_params['child_1']['name'] = $request->input('child_1_name');
					$other_params['child_1']['surname'] = $request->input('child_1_surname');
					$other_params['child_1']['birthdate'] = $request->input('child_1_birthdate');
					$other_params['child_1']['birthplace'] = $request->input('child_1_birthplace');
					$other_params['child_1']['nationality'] = $request->input('child_1_nationality');
					$other_params['child_1']['province'] = $request->input('child_1_province');
					$other_params['child_1']['zip'] = $request->input('child_1_zip');
					$other_params['child_1']['meals_allergies'] = $request->input('child_1_meals_allergies');
					$other_params['child_2']['name'] = $request->input('child_2_name');
					$other_params['child_2']['surname'] = $request->input('child_2_surname');
					$other_params['child_2']['birthdate'] = $request->input('child_2_birthdate');
					$other_params['child_2']['birthplace'] = $request->input('child_2_birthplace');
					$other_params['child_2']['nationality'] = $request->input('child_2_nationality');
					$other_params['child_2']['province'] = $request->input('child_2_province');
					$other_params['child_2']['zip'] = $request->input('child_2_zip');
					$other_params['child_2']['meals_allergies'] = $request->input('child_2_meals_allergies');
					$rpeople->ser_other_params = serialize($other_params);
					$rpeople->save();
					/*
					if(App::environment() == 'production'){
						//Send email to user
						$mail = Mail::to($rpeople->email)
							  ->send(new SendMailNotificationToUser(['title' => 'Conferma registrazione evento: '.$event->name, 'event_name' => $event->name]) );
						//Send email to event admin
						Mail::to('ritiri.ammaitalia@gmail.com')
							  ->send( new SendMailNotificationToAdmin(['name' => $rpeople->name, 'surname' => $rpeople->surname, 'event_name' => $event->name]));
					}
					 */
					$view_success = 'event_save_registration_success';
					$email_type_event = 'al Ritiro dallo staff';
					$email_signature = 'Team Ritiri MA Center Italy';
				break;
				case 2:
					$rpeople = new RegisteredPeople();
					$rpeople->event_id = $event->id;
					$rpeople->name = $request->input('name');
					$rpeople->spiritual_name = $request->input('spiritual_name');
					$rpeople->surname = $request->input('surname');
					$rpeople->sex = $request->input('sex');
					$rpeople->age = $request->input('age');
					//$rpeople->birthday = !empty($request->input('birthdate')) ? implode("-", array_reverse(explode("-", $request->input('birthdate')))) : NULL;
					//$rpeople->birthplace = $request->input('birthplace');
					$rpeople->nationality = $request->input('nationality');
					//$rpeople->zip = $request->input('zip');
					$rpeople->city = $request->input('city');
					$rpeople->province = $request->input('province');
					$rpeople->email = $request->input('email');
					$rpeople->mobile = $request->input('mobile');
					$rpeople->telephone = !empty($request->input('telephone')) ? $request->input('telephone') : '';
					$other_params = Array();
					if($request->input('event_1') !== null){
						$other_params['event_1'] = $request->input('event_1');
					}
					if($request->input('event_2') !== null){
						$other_params['event_2'] = $request->input('event_2');
					}
					if($request->input('event_3') != null){
						$other_params['event_3'] = $request->input('event_3');
					}
					if($request->input('event_4') !== null){
						$other_params['event_4'] = $request->input('event_4');
					}
					if($request->input('event_5') != null){
						$other_params['event_5'] = $request->input('event_5');
					}
					$other_params['meals_arrive_week'] = $request->input('meals_arrive_week');
					$other_params['meals_arrive_week'] = $request->input('meals_arrive_week');
					$other_params['meals_arrive_week'] = $request->input('meals_arrive_week');
					$other_params['meals_arrive_week'] = $request->input('meals_arrive_week');
					$other_params['meals_arrive_week'] = $request->input('meals_arrive_week');
					$other_params['meals_departure_week'] = $request->input('meals_departure_week');
					$other_params['meals_arrive_time'] = $request->input('meals_arrive_time');
					$other_params['meals_departure_time'] = $request->input('meals_departure_time');
					$other_params['meals_allergies'] = $request->input('meals_allergies');
					$other_params['accommodation'] = $request->input('accommodation');
					$other_params['linen'] = $request->input('linen');
					$other_params['note'] = $request->input('note');
					/*
					$other_params['block_seva_meal_distribution'] = $request->input('block_seva_meal_distribution');
					$other_params['block_seva_vegetable_preparation'] = $request->input('block_seva_vegetable_preparation');
					$other_params['block_seva_dish_washing'] = $request->input('block_seva_dish_washing');
					$other_params['block_seva_common_cleanup'] = $request->input('block_seva_common_cleanup');
					$other_params['block_seva_ext_cleanup'] = $request->input('block_seva_ext_cleanup');
					$other_params['block_seva_other'] = $request->input('block_seva_other');
					//$other_params['block_seva_retire'] = $request->input('block_seva_retire');
					 */
					$other_params['shuttle'] = $request->input('shuttle');
					$other_params['car_pooling'] = $request->input('car_pooling');
					$other_params['child_1']['name'] = $request->input('child_1_name');
					$other_params['child_1']['surname'] = $request->input('child_1_surname');
					$other_params['child_1']['age'] = $request->input('child_1_age');
					$other_params['child_1']['sex'] = $request->input('child_1_sex');
					$other_params['child_1']['meals_allergies'] = $request->input('child_1_meals_allergies');
					//$other_params['child_1']['birthdate'] = $request->input('child_1_birthdate');
					//$other_params['child_1']['birthplace'] = $request->input('child_1_birthplace');
					//$other_params['child_1']['nationality'] = $request->input('child_1_nationality');
					//$other_params['child_1']['province'] = $request->input('child_1_province');
					//$other_params['child_1']['zip'] = $request->input('child_1_zip');
					$other_params['child_2']['name'] = $request->input('child_2_name');
					$other_params['child_2']['surname'] = $request->input('child_2_surname');
					$other_params['child_2']['age'] = $request->input('child_2_age');
					$other_params['child_2']['sex'] = $request->input('child_2_sex');
					$other_params['child_2']['meals_allergies'] = $request->input('child_2_meals_allergies');
					//$other_params['child_2']['birthdate'] = $request->input('child_2_birthdate');
					//$other_params['child_2']['birthplace'] = $request->input('child_2_birthplace');
					//$other_params['child_2']['nationality'] = $request->input('child_2_nationality');
					//$other_params['child_2']['province'] = $request->input('child_2_province');
					//$other_params['child_2']['zip'] = $request->input('child_2_zip');
					$other_params['child_3']['name'] = $request->input('child_3_name');
					$other_params['child_3']['surname'] = $request->input('child_3_surname');
					$other_params['child_3']['age'] = $request->input('child_3_age');
					$other_params['child_3']['sex'] = $request->input('child_3_sex');
					$other_params['child_3']['meals_allergies'] = $request->input('child_3_meals_allergies');
					$rpeople->ser_other_params = serialize($other_params);
					$rpeople->save();
					$view_success = 'event_save_2_registration_success';
					$email_type_event = 'al Ritiro dallo staff';
					$email_signature = 'Team Ritiri MA Center Italy';
					$title = 'Ricezione registrazione: ';
				break;
			}
			$mail_to_send = $event->email;
			if(App::environment() == 'production'){
				if(empty($title)){
					$title = 'Conferma registrazione evento: ';
				}
				$time_description = $event->time_description;
				if($locale == 'en'){
					$title = 'Event registration confirmation: ';
				}
				//Send email to user
				$mail = Mail::to($rpeople->email)
					  ->send(new SendMailNotificationToUser(['title' => $title.$event->name, 'time_description' => $time_description, 'event_name' => $event->name, 'email_signature' => $email_signature, 'email_type_event' => $email_type_event, 'mail_to_send' => $mail_to_send, 'id' => $event->form_event_template_id]));
				//Send email to event admin
				$arr_mail_to_send = [$mail_to_send, 'iscrizione.centro@gmail.com'];
				$mail_to_send = $arr_mail_to_send;
				Mail::to($mail_to_send)
					  ->send( new SendMailNotificationToAdmin(['name' => $rpeople->name, 'surname' => $rpeople->surname, 'event_name' => $event->name, 'email_signature' => $email_signature, 'email_type_event' => $email_type_event, 'mail_to_send' => $mail_to_send]));
			}
					
			return redirect()->route($view_success, ['ok' => 'ok', 'email' => $event->email] );
		}
	}


	public function success(Request $request)
    {
        return view('event.success', ['page_title' => '', 'page_description' => '', 'page_motto' => '', 'email' => $request->input('email')]);
    }
	
	public function success_2(Request $request)
    {
        return view('event.success_2', ['page_title' => '', 'page_description' => '', 'page_motto' => '', 'email' => $request->input('email')]);
    }
	

    public function messages()
    {
        return [
            'sex.required|string|max:1' => '..',
        ];

    }
}
