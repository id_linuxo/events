<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\RegisteredPeople;
use App\Event;
use DB;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCsv(Request $request)
    {
        $registered_people = DB::table('registered_people')->where('event_id', $request->input('event_id'))->orderBy('created_at', 'desc')->get();
		$obj_event = DB::table('events')->where('id', $request->input('event_id'))->first();
		$form_event_template_id = $obj_event->form_event_template_id;
		if(!empty($form_event_template_id)){
	        $filename = '/tmp/registered_user'.time().'.csv';
    	    $handle = fopen($filename, 'w+');
			switch($form_event_template_id){
				default:
					$head_csv = Array(
									'Registration Date',
									'Name', 
									'Spiritual Name', 
									'Surname',
									'Sex',
									'BirthDay',
									'Birth Place',
									'Nationality',
									'City',
									'Province',
									'Zip code',
									'Email',
									'Mobile',
									'Telephone',
									'Arrive day',
									'Arrive time',
									'Departure day',
									'Departure time',
									'Food Allergies',
									'Accommodation needs',
									'Linen',
									'Meal distribution',
									'Meal preparation',
									'Dish washing',
									'Common areas cleanup',
									'External areas cleanup',
									'Parking',
									'Info for shuttle',
									'Child 1 Name',
									'Child 1 Surname',
									'Child 1 Birthdate',
									'Child 1 Birth place',
									'Child 1 Nationality',
									'Child 1 Province',
									'Child 1 Zip',
									'Child 1 food allergies',
									'Child 2 Name',
									'Child 2 Surname',
									'Child 2 Birthdate',
									'Child 2 Birth place',
									'Child 2 Nationality',
									'Child 2 Province',
									'Child 2 Zip',
									'Child 2 food allergies',
									'Privacy',
								);
				break;
				case 2:
					$head_csv = Array(
									'Registration Date',
									'Name', 
									'Spiritual Name', 
									'Surname',
									'Sex',
									'Age',
									//'BirthDay',
									//'Birth Place',
									'Nationality',
									'City',
									'Province',
									//'Zip code',
									'Email',
									'Mobile',
									'Telephone',
					);	
					if(!empty($obj_event->ser_other_params)){
						$settings = unserialize($obj_event->ser_other_params);
						if(!empty($settings) && !empty($settings['settings']) && !empty($settings['settings']['events'])){
							foreach($settings['settings']['events'] as $k => $v){
								if(!empty($v['label'])){
									$$k = true;//$event_<k>
									$head_csv[] = $v['label'];
								}
							}
						}
					};
					$head_csv_tail = Array(
									'Arrive day',
									'Arrive time',
									'Departure day',
									'Departure time',
									'Food Allergies',
									'Accommodation needs',
									'Linen',
									'Note',
									'Info for shuttle',
									'Privacy',
								);
					$head_csv = array_merge($head_csv, $head_csv_tail);
				break;
			}
			fputcsv($handle, $head_csv, ";",'"');
			switch($form_event_template_id){
				default:
					foreach($registered_people as $rp){
						$people['registration_date'] = $rp->created_at;
						$people['name'] = $rp->name;
						$people['spiritual_name'] = $rp->spiritual_name;
						$people['surname'] = $rp->surname;
						$people['sex'] = $rp->sex;
						$people['age'] = $rp->age;
						$people['birthday'] = $rp->birthday;
						$people['birthplace'] = $rp->birthplace;
						$people['nationality'] = $rp->nationality;
						$people['city'] = $rp->city;
						$people['province'] = $rp->province;
						$people['zip'] = (string)$rp->zip;
						$people['email'] = $rp->email;
						$people['mobile'] = $rp->mobile;
						$people['telephone'] = $rp->telephone;
						$ser_other_params = unserialize($rp->ser_other_params);
						$people['meals_arrive_week'] = $ser_other_params['meals_arrive_week'];
						$people['meals_arrive_time'] = $ser_other_params['meals_arrive_time'];
						$people['meals_departure_week'] = $ser_other_params['meals_departure_week'];
						$people['meals_departure_time'] = $ser_other_params['meals_departure_time'];
						$people['food_allergies'] = $ser_other_params['meals_allergies'];
						$people['acc_needs'] = $ser_other_params['accommodation'];
						$people['linen'] = $ser_other_params['linen'];
						$people['seva_meal_distribution'] = $ser_other_params['block_seva_meal_distribution'];
						$people['seva_meal_preparation'] = $ser_other_params['block_seva_meal_preparation'];
						$people['seva_dish_washing'] = $ser_other_params['block_seva_dish_washing'];
						$people['seva_common_cleanup'] = $ser_other_params['block_seva_common_cleanup'];
						$people['seva_ext_cleanup'] = $ser_other_params['block_seva_ext_cleanup'];
						$people['seva_parking'] = $ser_other_params['block_seva_parking'];
						$people['shuttle'] = $ser_other_params['shuttle'];
						$people['child_1_name'] = $ser_other_params['child_1']['name'];
						$people['child_1_surname'] = $ser_other_params['child_1']['surname'];
						$people['child_1_birthdate'] = $ser_other_params['child_1']['birthdate'];
						$people['child_1_birthplace'] = $ser_other_params['child_1']['birthplace'];
						$people['child_1_nationality'] = $ser_other_params['child_1']['nationality'];
						$people['child_1_province'] = $ser_other_params['child_1']['province'];
						$people['child_1_zip'] = $ser_other_params['child_1']['zip'];
						$people['child_1_meals_allergies'] = $ser_other_params['child_1']['meals_allergies'];
						$people['child_2_name'] = $ser_other_params['child_2']['name'];
						$people['child_2_surname'] = $ser_other_params['child_2']['surname'];
						$people['child_2_birthdate'] = $ser_other_params['child_2']['birthdate'];
						$people['child_2_birthplace'] = $ser_other_params['child_2']['birthplace'];
						$people['child_2_nationality'] = $ser_other_params['child_2']['nationality'];
						$people['child_2_province'] = $ser_other_params['child_2']['province'];
						$people['child_2_zip'] = $ser_other_params['child_2']['zip'];
						$people['child_2_meals_allergies'] = $ser_other_params['child_2']['meals_allergies'];
						$people['privacy'] = 1;
						fputcsv($handle, $people, ";",'"');
					}
				break;
				case 2:
					foreach($registered_people as $rp){
						$people['registration_date'] = $rp->created_at;
						$people['name'] = $rp->name;
						$people['spiritual_name'] = $rp->spiritual_name;
						$people['surname'] = $rp->surname;
						$people['sex'] = $rp->sex;
						$people['age'] = $rp->age;
						//$people['birthday'] = $rp->birthday;
						//$people['birthplace'] = $rp->birthplace;
						$people['nationality'] = $rp->nationality;
						$people['city'] = $rp->city;
						$people['province'] = $rp->province;
						//$people['zip'] = (string)$rp->zip;
						$people['email'] = $rp->email;
						$people['mobile'] = $rp->mobile;
						$people['telephone'] = $rp->telephone;
						$ser_other_params = unserialize($rp->ser_other_params);
						if(!empty($event_1)){
							$people['event_1'] = !empty($ser_other_params['event_1']) ? $ser_other_params['event_1'] : '';
						}
						if(!empty($event_2)){
							$people['event_2'] = !empty($ser_other_params['event_2']) ? $ser_other_params['event_2'] : '';
						}
						if(!empty($event_3)){
							$people['event_3'] = !empty($ser_other_params['event_3']) ? $ser_other_params['event_3'] : '';
						}
						if(!empty($event_4)){
							$people['event_4'] = !empty($ser_other_params['event_4']) ? $ser_other_params['event_4'] : '';
						}
						if(!empty($event_5)){
							$people['event_5'] = !empty($ser_other_params['event_5']) ? $ser_other_params['event_5'] : '';
						}
						$people['meals_arrive_week'] = $ser_other_params['meals_arrive_week'];
						$people['meals_arrive_time'] = $ser_other_params['meals_arrive_time'];
						$people['meals_departure_week'] = $ser_other_params['meals_departure_week'];
						$people['meals_departure_time'] = $ser_other_params['meals_departure_time'];
						$people['food_allergies'] = $ser_other_params['meals_allergies'];
						$people['acc_needs'] = $ser_other_params['accommodation'];
						$people['linen'] = $ser_other_params['linen'];
						$people['note'] = $ser_other_params['note'];
						$people['shuttle'] = $ser_other_params['shuttle'];
						$people['privacy'] = 1;
						fputcsv($handle, $people, ";",'"');
						if(!empty($ser_other_params['child_1']['name']) || !empty($ser_other_params['child_1']['surname'])){
							$child['label'] = 'Child 1';
							$child['name'] = $ser_other_params['child_1']['name'];
							$child['empty'] = '';
							$child['surname'] = $ser_other_params['child_1']['surname'];
							$child['sex'] = $ser_other_params['child_1']['sex'];
							$child['age'] = $ser_other_params['child_1']['age'];
							$child['meals_allergies'] = 'Allergy: '.$ser_other_params['child_1']['meals_allergies'];
							fputcsv($handle, $child, ";",'"');
						}
						if(!empty($ser_other_params['child_2']['name']) || !empty($ser_other_params['child_2']['surname'])){
							$child['label'] = 'Child 2';
							$child['name'] = $ser_other_params['child_2']['name'];
							$child['empty'] = '';
							$child['surname'] = $ser_other_params['child_2']['surname'];
							$child['sex'] = $ser_other_params['child_2']['sex'];
							$child['age'] = $ser_other_params['child_2']['age'];
							$child['meals_allergies'] = 'Allergy: '.$ser_other_params['child_2']['meals_allergies'];
							fputcsv($handle, $child, ";",'"');
						}
						if(!empty($ser_other_params['child_3']['name']) || !empty($ser_other_params['child_3']['surname'])){
							$child['label'] = 'Child 3';
							$child['name'] = $ser_other_params['child_3']['name'];
							$child['empty'] = '';
							$child['surname'] = $ser_other_params['child_3']['surname'];
							$child['sex'] = $ser_other_params['child_3']['sex'];
							$child['age'] = $ser_other_params['child_3']['age'];
							$child['meals_allergies'] = 'Allergy: '.$ser_other_params['child_3']['meals_allergies'];
							fputcsv($handle, $child, ";",'"');
						}
					}
				break;
			}
			fclose($handle);
	        echo file_get_contents($filename);
		}
    }    

    public function eventEnableDisable(Request $request)
    {
        $ok = false;
        if(!empty($request->event_id)){
            $event = Event::find($request->event_id);
            $request->actual_event_status == 0 ? $event->status = 1 : $event->status = 0;
            $event->save();
            $ok = true;
        }
        if($ok){
            echo 'Il modulo è stato '.($request->actual_event_status == 0 ? 'attivato ' : 'disattivato ').'correttamente';
        } else {
            echo 'Si è verificato un errore';
        }
    }

	public function eventHideShow(Request $request)
	{
        $ok = false;
        if(!empty($request->event_id)){
			$event = Event::find($request->event_id);
			if($request->actual_event_status == 0) {
            	$event->status = -1; 
			} 
			if($request->actual_event_status == -1){
				$event->status = 0;
			}
            $event->save();
            $ok = true;
        }
        if($ok){
            echo 'Il modulo è stato '.($request->actual_event_status == 0 ? 'nascosto ' : 'reso visibile ').'correttamente';
        } else {
            echo 'Si è verificato un errore';
        }

	}
	
	public function save_event_settings(Request $request)
	{
		$ok = false;
		if(!empty($request->event_id)){	
			$arr_settings = ['events' => [
										'event_1' => ['label' => '', 'description' => '', 'start' => '2023-01-01', 'end' => '2023-01-01', 'alert' => ''],
										'event_2' => ['label' => '', 'description' => '', 'start' => '2023-01-01', 'end' => '2023-01-01', 'alert' => ''],
										'event_3' => ['label' => '', 'description' => '', 'start' => '2023-01-01', 'end' => '2023-02-01', 'alert' => ''],
										'event_4' => ['label' => '', 'description' => '', 'start' => '2023-01-01', 'end' => '2023-03-01', 'alert' => ''],
										'event_5' => ['label' => '', 'description' => '', 'start' => '2023-01-11', 'end' => '2023-03-01', 'alert' => ''],
										],
						'alert_sleeping' => '',
						'hide_sleeping' => '',
			];
			
			$en = 1;
			foreach($arr_settings['events']	as $k => $event){
				//Label
				$label_param = "event_label_$en";
				if(!empty($request->$label_param)){
					$arr_settings['events'][$k]['label'] = $request->$label_param;
				}	
				//Description
				$description_param = "event_title_$en";
				if(!empty($request->$description_param)){
					$arr_settings['events'][$k]['description'] = $request->$description_param;
				}	
				//Start
				$start_param = "event_date_start_$en";
				if(!empty($request->$start_param)){
					$arr_settings['events'][$k]['start'] = $request->$start_param;
				}	
				//Alert
				$alert_param = "event_alert_$en";
				if(!empty($request->$alert_param)){
					$arr_settings['events'][$k]['alert'] .= $request->$alert_param;
				}
				$en++;
			}
			
			if(!empty($request->alert_accommodation)){
				$arr_settings['alert_sleeping'] = $request->alert_accommodation;
			}
			if(!empty($request->hide_accommodation)){
				$arr_settings['hide_sleeping'] = $request->hide_accommodation;
			}
			//print_r($arr_settings);
			
			$event = Event::find($request->event_id);
			$event->ser_other_params = serialize(['settings' => $arr_settings] );
			$event->save();
			$ok = true;
		}
		
		if($ok){
            echo 'Le impostazioni sono state salvate correttamente';
        } else {
            echo 'Si è verificato un errore. Riprovare a salvare oppure contattare aministratore.';
        }

	}
	
	public function viewCsv(Request $request){
        $registered_people = DB::table('registered_people')->where('event_id', $request->input('event_id'))->orderBy('created_at', 'desc')->get();
		$obj_event = DB::table('events')->where('id', $request->input('event_id'))->first();
		$form_event_template_id = $obj_event->form_event_template_id;
		if(!empty($form_event_template_id)){
			switch($form_event_template_id){
				default:
					$head_row_table_registered = Array(
									'<th>Registration Date</th>',
									'<th>Name</th>', 
									'<th>Spiritual Name</th>', 
									'<th>Surname</th>',
									'<th>Sex</th>',
									'<th>Age</th>',
									//'<th>BirthDay</th>',
									//'<th>Birth Place</th>',
									'<th>Nationality</th>',
									'<th>City</th>',
									'<th>Province</th>',
									//'<th>Zip code</th>',
									'<th>Email</th>',
									'<th>Mobile</th>',
									'<th>Telephone</th>',
					);	
					if(!empty($obj_event->ser_other_params)){
						$settings = unserialize($obj_event->ser_other_params);
						if(!empty($settings) && !empty($settings['settings']) && !empty($settings['settings']['events'])){
							foreach($settings['settings']['events'] as $k => $v){
								if(!empty($v['label'])){
									$$k = true;//$event_<k>
									$head_row_table_registered[] = '<th>'.$v['label'].'</th>';
								}
							}
						}
					};
					$head_row_table_registered_tail = Array(
									'<th>Arrive day</th>',
									'<th>Arrive time</th>',
									'<th>Departure day</th>',
									'<th>Departure time</th>',
									'<th>Food Allergies</th>',
									'<th>Accommodation needs</th>',
									'<th>Linen</th>',
									'<th>Note</th>',
									'<th>Info for shuttle</th>',
									'<th>Privacy</th>',
								);
					$head_row_table_registered = array_merge($head_row_table_registered, $head_row_table_registered_tail);
				break;
			}
			$head_table_registered = '<tr>'.implode($head_row_table_registered).'</tr>';
			//fputcsv($handle, $head_csv, ";",'"');
			switch($form_event_template_id){
				default:
				$rows_table_registered = '';
					foreach($registered_people as $rp){
						$people['registration_date'] = '<td>'.$rp->created_at.'</td>';
						$people['name'] = '<td>'.$rp->name.'</td>';
						$people['spiritual_name'] = '<td>'.$rp->spiritual_name.'</td>';
						$people['surname'] = '<td>'.$rp->surname.'</td>';
						$people['sex'] = '<td>'.$rp->sex.'</td>';
						$people['age'] = '<td>'.$rp->age.'</td>';
						//$people['birthday'] = '<td>'.$rp->birthday.'</td>';
						//$people['birthplace'] = '<td>'.$rp->birthplace.'</td>';
						$people['nationality'] = '<td>'.$rp->nationality.'</td>';
						$people['city'] = '<td>'.$rp->city.'</td>';
						$people['province'] = '<td>'.$rp->province.'</td>';
						//$people['zip'] = '<td>'.(string)$rp->zip.'</td>';
						$people['email'] = '<td>'.$rp->email.'</td>';
						$people['mobile'] = '<td>'.$rp->mobile.'</td>';
						$people['telephone'] = '<td>'.$rp->telephone.'</td>';
						$ser_other_params = unserialize($rp->ser_other_params);
						if(!empty($event_1)){
							$people['event_1'] = !empty($ser_other_params['event_1']) ? '<td>'.$ser_other_params['event_1'].'</td>' : '<td>'.''.'</td>';
						}
						if(!empty($event_2)){
							$people['event_2'] = !empty($ser_other_params['event_2']) ? '<td>'.$ser_other_params['event_2'].'</td>' : '<td>'.''.'</td>';
						}
						if(!empty($event_3)){
							$people['event_3'] = !empty($ser_other_params['event_3']) ? '<td>'.$ser_other_params['event_3'].'</td>' : '<td>'.''.'</td>';
						}
						if(!empty($event_4)){
							$people['event_4'] = !empty($ser_other_params['event_4']) ? '<td>'.$ser_other_params['event_4'].'</td>' : '<td>'.''.'</td>';
						}
						if(!empty($event_5)){
							$people['event_5'] = !empty($ser_other_params['event_5']) ? '<td>'.$ser_other_params['event_5'].'</td>' : '<td>'.''.'</td>';
						}
						$people['meals_arrive_week'] = '<td>'.$ser_other_params['meals_arrive_week'].'</td>';
						$people['meals_arrive_time'] = '<td>'.$ser_other_params['meals_arrive_time'].'</td>';
						$people['meals_departure_week'] = '<td>'.$ser_other_params['meals_departure_week'].'</td>';
						$people['meals_departure_time'] = '<td>'.$ser_other_params['meals_departure_time'].'</td>';
						$people['food_allergies'] = '<td>'.$ser_other_params['meals_allergies'].'</td>';
						$people['acc_needs'] = '<td>'.$ser_other_params['accommodation'].'</td>';
						$people['linen'] = '<td>'.$ser_other_params['linen'].'</td>';
						$people['note'] = '<td>'.$ser_other_params['note'].'</td>';
						$people['shuttle'] = '<td>'.$ser_other_params['shuttle'].'</td>';
						$people['privacy'] = '<td>1</td>';
						$rows_table_registered .= '<tr>'.implode($people).'</tr>';
						if(!empty($ser_other_params['child_1']['name']) || !empty($ser_other_params['child_1']['surname'])){
							$child = [];
							$child['label'] ='<td>'.'Child 1'.'</td>';
							$child['name'] = '<td>'.$ser_other_params['child_1']['name'].'</td>';
							$child['empty'] = '<td>&nbsp;</td>';
							$child['surname'] = '<td>'.$ser_other_params['child_1']['surname'].'</td>';
							$child['sex'] = '<td>'.$ser_other_params['child_1']['sex'].'</td>';
							$child['age'] = '<td>'.$ser_other_params['child_1']['age'].'</td>';
							$child['meals_allergies'] = '<td colspan="17">Allergy:&nbsp;'.$ser_other_params['child_1']['meals_allergies'].'</td>';
							$rows_table_registered .= '<tr>'.implode($child).'</tr>';
						}
						if(!empty($ser_other_params['child_2']['name']) || !empty($ser_other_params['child_2']['surname'])){
							$child = [];
							$child['label'] ='<td>'.'Child 2'.'</td>';
							$child['name'] = '<td>'.$ser_other_params['child_2']['name'].'</td>';
							$child['empty'] = '<td>&nbsp;</td>';
							$child['surname'] = '<td>'.$ser_other_params['child_2']['surname'].'</td>';
							$child['sex'] = '<td>'.$ser_other_params['child_2']['sex'].'</td>';
							$child['age'] = '<td>'.$ser_other_params['child_2']['age'].'</td>';
							$child['meals_allergies'] = '<td colspan="17">Allergy:&nbsp;'.$ser_other_params['child_2']['meals_allergies'].'</td>';
							$rows_table_registered .= '<tr>'.implode($child).'</tr>';
						}
						if(!empty($ser_other_params['child_3']['name']) || !empty($ser_other_params['child_3']['surname'])){
							$child = [];
							$child['label'] ='<td>'.'Child 3'.'</td>';
							$child['name'] = '<td>'.$ser_other_params['child_3']['name'].'</td>';
							$child['empty'] = '<td>&nbsp;</td>';
							$child['surname'] = '<td>'.$ser_other_params['child_3']['surname'].'</td>';
							$child['sex'] = '<td>'.$ser_other_params['child_3']['sex'].'</td>';
							$child['age'] = '<td>'.$ser_other_params['child_3']['age'].'</td>';
							$child['meals_allergies'] = '<td colspan="17">Allergy:&nbsp;'.$ser_other_params['child_3']['meals_allergies'].'</td>';
							$rows_table_registered .= '<tr>'.implode($child).'</tr>';
						}
					}
				break;
			}
			$table_style = "
<style>
 th, td {
  padding: 5px;
}
tr {
border-bottom: 1pt solid black;
}
</style>";
			return $table_style.'<table width="200%" style="white-space: nowrap">'.$head_table_registered .$rows_table_registered.'</table>';
		}
	}
	
	public function delete_rp_from_date(Request $request)
	{
		$ok = false;
		if(empty($request->event_id) || empty($request->from_date)){
			echo 'L\id dell\'evento oppure la data sono vuoti';
			exit;
		}

		$ok = RegisteredPeople::where('created_at', '<=', $request->from_date)->where('event_id', $request->event_id)->delete();
		
		if($ok !== 'false'){
			echo 'Sono stati cancellati '.$ok.' registrati';
		} else {
			echo 'Si è verificato un errore. Riprova oppure contatta l\'amministratore';	
		}
	}

}
