<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Event;
use DB;

class EventController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id = 0)
    {
        $registered_people = DB::table('registered_people')->where('event_id', $id)->get();
        $num_registered_people = count($registered_people);
        $event = Event::find($id);
        $page_title = $event->name; 
        $page_description = $event->description; 
        $user = User::find(\Auth::user()->id);
		if($user->id > 2){//Guest
        	$Objevents = Event::where('status', '!=', -1)->orderBy('created_at', 'desc')->get();
		} else {//Admin
        	$Objevents = Event::all()->sortByDesc('created_at');
		}
        return view('admin.event', compact('num_registered_people', 'user', 'Objevents', 'page_title', 'page_description', 'event'));
    }

	public function settings($id = 0){
        $user = User::find(\Auth::user()->id);
		if($user->id > 2){//Guest
        	$Objevents = Event::where('status', '!=', -1)->orderBy('created_at', 'desc')->get();
		} else {//Admin
        	$Objevents = Event::all()->sortByDesc('created_at');
		}
        $event = Event::find($id);
        $page_title = $event->name; 
        $page_description = $event->description; 
		$arr_settings = [];
		if(!empty($event)){
			if(!empty($event->ser_other_params)){
				$other_params = unserialize($event->ser_other_params);
				if(!empty($other_params && !empty($other_params['settings']))){
					$arr_settings = $other_params['settings'];
				}
			}
		}
        return view('admin.event-settings', compact('arr_settings', 'user', 'Objevents', 'page_title', 'page_description', 'event'));
	}

}
