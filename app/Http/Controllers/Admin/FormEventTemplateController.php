<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FormEventTemplate;

class FormEventTemplateController extends Controller
{
    //
    public function createDefault(Request $request)
    {
		if(empty($request->id_template)){
			$arr_default_params = Array(
				'name' => Array('type' => 'string', 'it_label' => 'Nome', 'en_label' => 'Name', 'mandatory' => 1),
				'spiritual_name' => Array('type' => 'string', 'it_label' => 'Nome spirituale', 'en_label' => 'Spiritual name',),
				'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'Surname', 'mandatory' => 1),
				'sex' => Array('type' => 'radio', 'it_label' => 'Sesso', 'en_label' => 'Sex', 'mandatory' => 1, 'values' => Array('Donna' => 'F', 'Uomo' => 'M'), 'mandatory' => 1),
				'birthdate' => Array('type' => 'date', 'it_label' => 'Data di nascita <i>(gg-mm-aaaa)</i>', 'en_label' => 'Birthday', 'mandatory' =>1),
				'birthplace' => Array('type' => 'string', 'it_label' => 'Luogo di nascita', 'en_label' => 'Birth place', 'mandatory' => 1),
				'nationality' => Array('type' => 'string', 'it_label' => 'Nazionalit&agrave;', 'en_label' => 'Nationality', 'mandatory' => 1),
				'city' => Array('type' => 'string', 'it_label' => 'Citt&agrave; di residenza', 'en_label' => 'City', 'mandatory' => 1),
				'province' => Array('type' => 'string', 'it_label' => 'Provincia di residenza', 'en_label' => 'Province', 'mandatory' => 1),
				'zip' => Array('type' => 'string', 'it_label' => 'CAP', 'en_label' => 'Zip code', 'mandatory' => 1),
				'email' => Array('type' => 'email', 'it_label' => 'Email', 'en_label' => 'Email', 'mandatory' => 1),
				'mobile' => Array('type' => 'string', 'it_label' => 'Cellulare', 'en_label' => 'Mobile', 'mandatory' => 1),
				'telephone' => Array('type' => 'string', 'it_label' => 'Telefono', 'en_label' => 'Telefono'),
				'meals' => Array('type' => 'row', 'it_label' => 'REFEZIONE', 'it_sub_label' => '(<i>I principali alimenti che compongono i men&ugrave; sono di Origine Biologica e/o di Filiera Corta e rispettano una dieta di tipo Vegetariano. La tua iscrizione al ritiro, comprende tutti i pasti, <u>dalla cena della domenica al pranzo del mercoledì.</u>. Per preparare il giusto numero di coperti ed evitare sprechi di cibo o mancanze, &egrave; assolutamentei necessario confermare la tua presenza nel: Giorno e ora di arrivo - Giorno e ora di partenza. In caso di eventuali modifiche sulla tua presenza ti preghiamo di avvisarci tempestivamente. Grazie.)</i>', 'en_label' => 'Meals'),
				'meals_arrive_week' => Array('type' => 'select_week', 'it_label' => 'Giorno di arrivo', 'en_label' => 'Arrive day', 'mandatory' => 1),
				'meals_arrive_time' => Array('type' => 'select_time', 'it_label' => 'Ora di arrivo', 'en_label' => 'Arrive time', 'values' => Array(8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24), 'mandatory' => 1),
				'meals_departure_week' => Array('type' => 'select_week', 'it_label' => 'Giorno di partenza', 'en_label' => 'Arrive day', 'mandatory' => 1),
				'meals_departure_time' => Array('type' => 'select_time', 'it_label' => 'Ora di partenza', 'en_label' => 'Arrive time', 'values' => Array(8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24), 'mandatory' => 1),
				'meals_allergies' => Array('type' => 'text', 'it_label' => '</br>Ti preghiamo di segnalare se nella tua dieta sono presenti allergie o intolleranze, faremo il possibile per venire incontro alle tue esigenze:', 'en_label' => 'Food allergies'),
				'accommodation' => Array('type' => 'text', 'it_label' => 'PERNOTTO E CAMERE:&nbsp;<span style="color:red">ATTENZIONE! SIAMO SPIACENTI MA NON CI SONO PIÙ POSTI PER DORMIRE AL CENTRO</span>', 'it_sub_label' => '(La sistemazione avverr&agrave; in camere femminili o maschili con bagno. Se hai importanti necessit&agrave;, ti preghiamo di segnalarlo. Faremo il possibile per venire incontro alle tue esigenze. <u>Prenderemo in esame solo le richieste pervenute in fase di iscrizione e faremo il possibile per accontentarvi</u>.)', 'en_label' => 'Accommodation', 'en_sub_label' => ''),
				'linen' => Array('type' => 'text', 'it_label' => 'BIANCHERIA', 'it_sub_label' => '(La biancheria da notte e quella da bagno NON sono compresi. Se hai necessità di noleggiare la biancheria, ti preghiamo di segnalarlo adesso in questo spazio, verrai informata/o se in questo Ritiro &egrave; possibile e l\'eventuale costo aggiuntivo. )', 'en_label' => 'Linen', 'en_sub_label' => ''),
				'pets' => Array('type' => 'row', 'it_label' => 'ANIMALI AL SEGUITO', 'it_sub_label' => '(Purtroppo al momento NON è possibile portare animali al seguito)', 'en_label' => 'Pets', 'en_sub_label' => ''),
				'setting_up' => Array('type' => 'row', 'it_label' => '<br>SEVA, SERVIZIO DISINTERESSATO DURANTE IL RITIRO RESIDENZIALE', 'it_sub_label' => '(Lo spirito con cui vengono organizzati i ritiri residenziali &egrave; lo stesso che anima il programma di Amma e i numerosi progetti: il servizio disinteressato (seva). Ogni ritiro &egrave; reso possibile grazie al contributo di ciascun partecipante: tutti insieme aderiamo ai momenti di servizio comunitario. A seguire un elenco di seva. Ti chiediamo di esprimere le tue preferenze. Al tuo arrivo in accoglienza ti verrà assegnato il servizio e il relativo orario. Invitiamo ogni partecipante ad aderire a un turno di seva. Precisiamo che i turni sono brevi e ben organizzati. Grazie in anticipo per il tuo supporto! ). <u>Attenzione! Selezionare almeno un seva dalla lista.</u>', 'en_label' => 'Setting up', 'en_sub_label' => ''),
				'block_seva_meal_distribution' => Array('type' => 'radio', 'it_label' => 'Seva distribuzione di un pasto', 'en_label' => 'Seva kitchen: Breakfast preparation and distribution', 'values' => ['No' => 0, 'Si' => 1]),
				'block_seva_meal_preparation' => Array('type' => 'radio', 'it_label' => 'Seva preparazione di un pasto', 'en_label' => 'Seva kitchen: Lunch preparation', 'values' => Array('No' => 0, 'Si' => 1)),
				'block_seva_dish_washing' => Array('type' => 'radio', 'it_label' => 'Seva lavaggio piatti', 'en_label' => 'Food preparation and pot washing Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				'block_seva_common_cleanup' => Array('type' => 'radio', 'it_label' => 'Seva pulizie parti comuni interne', 'en_label' => 'Help was dishes and dining room cleanup Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				'block_seva_ext_cleanup' => Array('type' => 'radio', 'it_label' => 'Seva pulizie parti esterne', 'en_label' => 'cleanup Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				'block_seva_parking' => Array('type' => 'radio', 'it_label' => 'Seva parcheggio', 'en_label' => 'Retreat Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				'info_1' => Array('type' => 'row', 'it_label' => 'L\'accoglienza avrà inizio domenica a partire dalle ore 14.Alle 19 verranno organizzati i bhajan serali a cui farà seguito la cena. Il ritiro inizierà lunedì con una giornata dedicata all\'AMRITA YOGA condotta da VINOD e proseguirà martedì e mercoledì con il programma di Swami Shubamritananda Puri.'), 
				'info_2' => Array('type' => 'row', 'it_label' => 'Per la giornata di AMRITA YOGA ti ricordiamo di portare il materassino per praticare lo yoga. Non dimenticarlo perch&egrave; al Centro non abbiamo a disposizione materassini da offrire.'), 
				'row_1' => Array('type' => 'row', 'it_label' => '</br>'),
				'retire_finish' => Array('type' => 'row', 'it_label' => 'Il ritiro finir&agrave; Mercoled&igrave; nel primo pomeriggio, dopo un momento di seva comunitario dedicato alla chiusura del ritiro. '), 
				'info_3' =>  Array('type' => 'row', 'it_label' => '</br>SERVIZIO NAVETTA STAZIONE ARQUATA SCRIVIA: se prevedi di arrivare in treno alla stazione di Arquata Scrivia e desideri usufruire del servizio navetta a fronte di un piccolo contributo, ti preghiamo di indicare qui il giorno e l’orario di arrivo / partenza per poter organizzare al meglio il servizio. Puoi visualizzare i dettagli del servizio navetta direttamente sul sito.', 'en_label' => 'Car pooling', 'en_sub_label' => ''),
				'shuttle' => Array('type' => 'text', 'it_label' => 'INSERIRE GIORNO E ORA ARRIVO E PARTENZA', 'it_sub_label' => '', 'en_label' => 'Bus', 'en_sub_label' => ''),
				'row_2' => Array('type' => 'row', 'it_label' => 'ISCRIZIONE PER MINORI DI 16 ANNI'),
				'child_1' => Array('type' => 'child', 'it_label' => '&nbsp;', 'en_label' => 'Child', 'values' => Array(
																	'name' => Array('type' => 'string', 'it_label' => '<br>Nome', 'en_label' => 'Name'),
																	'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'surname'),
																	'birthdate' => Array('type' => 'date', 'it_label' => 'Data di nascita <i>(gg-mm-aaaa)</i>', 'en_label' => 'Birthday'),
																	'birthplace' => Array('type' => 'string', 'it_label' => 'Luogo di nascita', 'en_label' => 'Birth place'),
																	'nationality' => Array('type' => 'string', 'it_label' => 'Nazionalit&agrave;', 'en_label' => 'Nationality'),
																	'province' => Array('type' => 'string', 'it_label' => 'Provincia di residenza', 'en_label' => 'Province'),
																	'zip' => Array('type' => 'string', 'it_label' => 'CAP', 'en_label' => 'Zip code'),
																	'meals_allergies' => Array('type' => 'text', 'it_label' => 'Dieta: Segnala per cortesia intolleranze o allergie del minore, Grazie', 'en_label' => 'Food allergies'),
																)
								  ),
				'child_2' => Array('type' => 'child', 'it_label' => '&nbsp;', 'en_label' => 'Child', 'values' => Array(
																	'name' => Array('type' => 'string', 'it_label' => 'Nome', 'en_label' => 'Name'),
																	'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'surname'),
																	'birthdate' => Array('type' => 'date', 'it_label' => 'Data di nascita <i>(gg-mm-aaaa)</i>', 'en_label' => 'Birthday'),
																	'birthplace' => Array('type' => 'string', 'it_label' => 'Luogo di nascita', 'en_label' => 'Birth place'),
																	'nationality' => Array('type' => 'string', 'it_label' => 'Nazionalit&agrave;', 'en_label' => 'Nationality'),
																	'province' => Array('type' => 'string', 'it_label' => 'Provincia di residenza', 'en_label' => 'Province'),
																	'zip' => Array('type' => 'string', 'it_label' => 'CAP', 'en_label' => 'Zip code'),
																	'meals_allergies' => Array('type' => 'text', 'it_label' => 'Dieta: Segnala per cortesia intolleranze o allergie del minore, Grazie', 'en_label' => 'Food allergies'),
																)
								  ),
				
				'privacy' => Array('type' => 'checkbox', 'it_label' => 'Privacy</br><i>(Dichiaro espressamente di aver letto le norme sulla privacy e dare il consenso al trattamento dei dati personali ai sensi dell&#39;art.13 e seg. del D.Lgs.n.196/2003)</i></br>&nbsp;-&nbsp;<a href="http://www.amma-italia.it/privacy/" target="_blank">Privacy Policy</a>', 'en_label' => 'Privacy', 'values' => Array('privacy' => 1), 'mandatory' => 1)
			);
		} else {
			$method = "template_$request->id_template";
			if(method_exists($this, $method)){
				$arr_default_params = $this->$method();
			} else {
				echo "Method $method() doesn't exist";
				exit;
			}
		}	
		$ser_params = serialize($arr_default_params);

		if(!empty($request->id)){
			$fet = FormEventTemplate::find($request->id);
		} else {
			$fet = new FormEventTemplate();
			if(!empty($request->id_template)){
				$fet->id = $request->id_template;
			}
		}
		$fet->ser_params = $ser_params;
		$fet->description = 'Template '.$request->id;
		$fet->save();
	}    


	//Soggiornare al centro
	public function template_2()
	{
			$arr_params = Array(
				//'event_1' => Array('type' => 'radio', 'it_label' => '<span style="color:red"><u>Vuoi partecipare al corso IAM del 8 aprile</u>&nbsp;&nbsp;<!--span class="blink_me"><strong><u>ATTENZIONE! POSTI ESAURITI</u></strong></span>&nbsp;</span-->', 'en_label' => 'event_1', 'values' => ['No' => 0, 'Si' => 1], 'mandatory' => 1),
				'name' => Array('type' => 'string', 'it_label' => 'Nome', 'en_label' => 'Name', 'mandatory' => 1),
				'spiritual_name' => Array('type' => 'string', 'it_label' => 'Nome spirituale', 'en_label' => 'Spiritual name',),
				'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'Surname', 'mandatory' => 1),
				'row_0' => Array('type' => 'row', 'it_label' => '</br>'),
				'sex' => Array('type' => 'radio', 'it_label' => 'Sesso', 'en_label' => 'Sex', 'mandatory' => 1, 'values' => Array('Donna' => 'F', 'Uomo' => 'M'), 'mandatory' => 1),
				'age' => Array('type' => 'number', 'it_label' => 'Et&agrave;', 'en_label' => 'Age', 'mandatory' =>1),
				//'birthdate' => Array('type' => 'date', 'it_label' => 'Data di nascita <i>(gg-mm-aaaa)</i>', 'en_label' => 'Birthday', 'mandatory' =>1),
				//'birthplace' => Array('type' => 'string', 'it_label' => 'Luogo di nascita', 'en_label' => 'Birth place', 'mandatory' => 1),
				'nationality' => Array('type' => 'string', 'it_label' => 'Nazionalit&agrave;', 'en_label' => 'Nationality', 'mandatory' => 1),
				'city' => Array('type' => 'string', 'it_label' => 'Citt&agrave; di residenza', 'en_label' => 'City', 'mandatory' => 1),
				'province' => Array('type' => 'string', 'it_label' => 'Provincia di residenza', 'en_label' => 'Province', 'mandatory' => 1),
				//'zip' => Array('type' => 'string', 'it_label' => 'CAP', 'en_label' => 'Zip code', 'mandatory' => 1),
				'email' => Array('type' => 'email', 'it_label' => 'Email', 'en_label' => 'Email', 'mandatory' => 1),
				'mobile' => Array('type' => 'string', 'it_label' => 'Cellulare', 'en_label' => 'Mobile', 'mandatory' => 1),
				'telephone' => Array('type' => 'string', 'it_label' => 'Telefono', 'en_label' => 'Telefono'),
				'meals_arrive_week' => Array('type' => 'date', 'it_label' => 'Giorno di arrivo <i>(gg-mm-aaaa)</i>', 'en_label' => 'Arrive day', 'values' => Array('giovedi', 'venerdi', 'sabato', 'domenica'), 'mandatory' => 1),
				//'info_1' => Array('type' => 'row', 'it_label' => '<br><span style="color:red;font-weight:bold">ATTENZIONE!&nbsp;&nbsp;<u>Posti esauriti dal 27 al 29 GENNAIO compresi</u></span>'), 
				'meals_arrive_time' => Array('type' => 'select_time', 'it_label' => 'Ora di arrivo', 'en_label' => 'Arrive time', 'values' => Array(8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24), 'mandatory' => 1),
				'meals_departure_week' => Array('type' => 'date', 'it_label' => 'Giorno di partenza <i>(gg-mm-aaaa)</i>', 'en_label' => 'Arrive day', 'values' => Array('venerdi', 'sabato', 'domenica'), 'mandatory' => 1),
				'meals_departure_time' => Array('type' => 'select_time', 'it_label' => 'Ora di partenza', 'en_label' => 'Arrive time', 'values' => Array(8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24), 'mandatory' => 1),
				//'row_alert' => Array('type' => 'row', 'it_label' => '<span style="color:red">ATTENZIONE!: Posti per dormire al centro esauriti dal 17 al 19 febbraio compresi</span>'),
				//'accommodation' => Array('type' => 'text', 'it_label' => 'PERNOTTO E CAMERE:&nbsp;<span style="color:red">ATTENZIONE! SIAMO SPIACENTI MA NON CI SONO PIÙ POSTI PER DORMIRE AL CENTRO</span>', 'it_sub_label' => '(La sistemazione avverr&agrave; in camere femminili o maschili con bagno. Se hai importanti necessit&agrave;, ti preghiamo di segnalarlo. Faremo il possibile per venire incontro alle tue esigenze. <u>Prenderemo in esame solo le richieste pervenute in fase di iscrizione e faremo il possibile per accontentarvi</u>.)', 'en_label' => 'Accommodation', 'en_sub_label' => ''),
				'row_1' => Array('type' => 'row', 'it_label' => '</br>'),
				'accommodation' => Array('type' => 'radio', 'it_label' => 'PERNOTTAMENTO AL CENTRO', 'en_label' => '', 'values' => ['No' => 0, 'Si' => 1], 'mandatory' => 1),
				'linen' => Array('type' => 'text', 'it_label' => 'BIANCHERIA', 'it_sub_label' => '(La biancheria da notte e quella da bagno non sono comprese. Se hai la necessit&agrave; di noleggiare la biancheria, ti preghiamo di indicarlo nell\'apposito spazio sottostante. Ti comunicheremo la disponibilità e l\'eventuale costo aggiuntivo)', 'en_label' => 'Linen', 'en_sub_label' => ''),
				'meals_allergies' => Array('type' => 'text', 'it_label' => '', 'it_sub_label' => '<b>I pasti serviti sono di tipo vegetariano.  Ti preghiamo di segnalare se hai necessità particolari per la tua dieta.Faremo il possibile per venire incontro alle tue esigenze.<br> Ti segnaliamo che la cucina non è autorizzata per pasti per celiaci.</b>', 'en_label' => 'Meals'),
				//'meals_allergies' => Array('type' => 'text', 'it_label' => '', 'en_label' => ''),
				'setting_up' => Array('type' => 'row', 'it_label' => '<br>SEVA, SERVIZIO DISINTERESSATO', 'it_sub_label' => 'Lo spirito che anima il Centro e tutti coloro che lo frequentano è  il servizio disinteressato (seva), pratica molto cara ad Amma perchè permette di aprire il cuore e trascendere le proprie preferenze e avversioni. Ognuno è chiamato a offrire il proprio contributo. Al tuo arrivo in accoglienza ti verrà indicato il seva che svolgerai e il referente a cui fare riferimento. Grazie in anticipo per il tuo contributo, è per noi molto importante!', 'en_label' => 'Setting up', 'en_sub_label' => ''),
				//'block_seva_meal_distribution' => Array('type' => 'radio', 'it_label' => 'Seva distribuzione cibo', 'en_label' => 'Seva kitchen: Breakfast preparation and distribution', 'values' => ['No' => 0, 'Si' => 1]),
				//'block_seva_vegetable_preparation' => Array('type' => 'radio', 'it_label' => 'Seva taglio verdure', 'en_label' => 'Seva kitchen: Lunch preparation', 'values' => Array('No' => 0, 'Si' => 1)),
				//'block_seva_dish_washing' => Array('type' => 'radio', 'it_label' => 'Seva lavaggio piatti', 'en_label' => 'Food preparation and pot washing Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				//'block_seva_bar' => Array('type' => 'radio', 'it_label' => 'Seva Bar e Pasticceria', 'en_label' => 'Bar and Cake seva', 'values' => Array('No' => 0, 'Si' => 1)),
				//'block_seva_common_cleanup' => Array('type' => 'radio', 'it_label' => 'Seva pulizie parti comuni interne', 'en_label' => 'Help was dishes and dining room cleanup Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				//'block_seva_ext_cleanup' => Array('type' => 'radio', 'it_label' => 'Seva pulizie parti esterne', 'en_label' => 'cleanup Seva', 'values' => Array('No' => 0, 'Si' => 1)),
				//'block_seva_other' => Array('type' => 'radio', 'it_label' => 'Altro           ', 'en_label' => 'other', 'values' => Array('No' => 0, 'Si' => 1)),
				'row_1' => Array('type' => 'row', 'it_label' => '</br>'),
				'info_3' =>  Array('type' => 'row', 'it_label' => '</br>SERVIZIO NAVETTA STAZIONE ARQUATA SCRIVIA: se prevedi di arrivare in treno alla stazione di Arquata Scrivia e desideri usufruire del servizio navetta a fronte di un piccolo contributo, ti preghiamo di indicare qui il giorno e l’orario di arrivo / partenza per poter organizzare al meglio il servizio.', 'en_label' => 'Car pooling', 'en_sub_label' => ''),
				'shuttle' => Array('type' => 'text', 'it_label' => 'INSERIRE GIORNO E ORA ARRIVO E PARTENZA', 'it_sub_label' => '', 'en_label' => 'Bus', 'en_sub_label' => ''),
				'note' => Array('type' => 'text', 'it_label' => 'NOTE: compila il campo seguente per richieste e segnalazioni particolari.', 'it_sub_label' => '', 'en_label' => 'Note', 'en_sub_label' => ''),
				'row_2' => Array('type' => 'row', 'it_label' => 'ISCRIZIONE PER I FIGLI'),
				'child_1' => Array('type' => 'child', 'it_label' => '&nbsp;', 'en_label' => 'Child', 'values' => Array(
																	'name' => Array('type' => 'string', 'it_label' => '<br>Nome', 'en_label' => 'Name'),
																	'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'surname'),
																	'sex' => Array('type' => 'radio', 'it_label' => 'Sesso', 'en_label' => 'Sex', 'mandatory' => 1, 'values' => Array('Femmina' => 'F', 'Maschio' => 'M'), 'mandatory' => 1),
																	'age' => Array('type' => 'number', 'it_label' => 'Et&agrave;', 'en_label' => 'Age'),
																	'meals_allergies' => Array('type' => 'text', 'it_label' => 'Ti preghiamo di segnalare se ha necessità particolari per la sua dieta.', 'en_label' => 'Food allergies'),
																)
								  ),
				'child_2' => Array('type' => 'child', 'it_label' => '&nbsp;', 'en_label' => 'Child', 'values' => Array(
																	'name' => Array('type' => 'string', 'it_label' => 'Nome', 'en_label' => 'Name'),
																	'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'surname'),
																	'age' => Array('type' => 'number', 'it_label' => 'Et&agrave;', 'en_label' => 'Age'),
																	'sex' => Array('type' => 'radio', 'it_label' => 'Sesso', 'en_label' => 'Sex', 'mandatory' => 1, 'values' => Array('Femmina' => 'F', 'Maschio' => 'M'), 'mandatory' => 1),
																	'meals_allergies' => Array('type' => 'text', 'it_label' => 'Ti preghiamo di segnalare se ha necessità particolari per la sua dieta.', 'en_label' => 'Food allergies'),
																)
								  ),
				'child_3' => Array('type' => 'child', 'it_label' => '&nbsp;', 'en_label' => 'Child', 'values' => Array(
																	'name' => Array('type' => 'string', 'it_label' => 'Nome', 'en_label' => 'Name'),
																	'surname' => Array('type' => 'string', 'it_label' => 'Cognome', 'en_label' => 'surname'),
																	'age' => Array('type' => 'number', 'it_label' => 'Et&agrave;', 'en_label' => 'Age'),
																	'sex' => Array('type' => 'radio', 'it_label' => 'Sesso', 'en_label' => 'Sex', 'mandatory' => 1, 'values' => Array('Femmina' => 'F', 'Maschio' => 'M'), 'mandatory' => 1),
																	'meals_allergies' => Array('type' => 'text', 'it_label' => 'Ti preghiamo di segnalare se ha necessità particolari per la sua dieta.', 'en_label' => 'Food allergies'),
																)
								  ),
				'privacy' => Array('type' => 'checkbox', 'it_label' => 'Privacy</br><i>(Dichiaro espressamente di aver letto le norme sulla privacy e dare il consenso al trattamento dei dati personali ai sensi dell&#39;art.13 e seg. del D.Lgs.n.196/2003)</i></br>&nbsp;-&nbsp;<a href="http://www.amma-italia.it/privacy/" target="_blank">Privacy Policy</a>', 'en_label' => 'Privacy', 'values' => Array('privacy' => 1), 'mandatory' => 1)
			);
			
			return $arr_params;
	}
	
}
