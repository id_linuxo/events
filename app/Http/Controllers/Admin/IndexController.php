<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\User;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(\Auth::user()->id);
		if($user->id > 2){//Guest
        	$Objevents = Event::where('status', '!=', -1)->orderBy('created_at', 'desc')->get();
		} else {//Admin
        	$Objevents = Event::all()->sortByDesc('created_at');
		}
        $page_title = 'Ciao '.$user->name.'! Seleziona un evento dal men&ugrave a sinistra';
        $page_description = '';
        return view('admin.index', compact('Objevents', 'user', 'page_title', 'page_description'));
    }
}
