<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormEventTemplate extends Model
{
    //

    /*
    * Gets all events that have the same event_template
    */
    public function events()
    {
        return $this->hasMany('App\FormEvent');
    }
}
