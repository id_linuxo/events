<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailNotificationToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.message_sent')
                    ->subject('Notifica: '.$this->event['event_name'])
                    ->with(['name' => $this->event['name'], 'surname' => $this->event['surname'], 'event_name' => $this->event['event_name'], 'email_signature' => $this->event['email_signature'], 'email_type_event' => $this->event['email_type_event'], 'mail_to_send' => $this->event['mail_to_send']]);
    }
}
