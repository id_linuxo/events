<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailNotificationToUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = 'emails.registration_success';//Default
        if(!empty($this->event['id'])){
            $view = 'emails.registration_success_'.$this->event['id'];
        }
        return $this->view($view)
                     ->subject('Registrazioni all\'evento: '.$this->event['event_name'].' - Amma Italia')
                     ->with(['title' => $this->event['title'], 'event_name' => $this->event['event_name'], 'email_signature' => $this->event['email_signature'], 'email_type_event' => $this->event['email_type_event'], 'mail_to_send' => $this->event['mail_to_send'], 'time_description' => $this->event['time_description']]);
    }
}
