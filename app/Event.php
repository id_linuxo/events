<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public function form_event_templates()
    {
        return $this->belongsTo('App\FormEventTemplate', 'form_event_template_id');
    }
    
    /*
    * Get all registered people to event
    */
    public function registered_people()
    {
        return $this->hasMany('App\RegisteredPeople');
    } 
}
