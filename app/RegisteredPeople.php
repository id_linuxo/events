<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredPeople extends Model
{
    protected $table = 'registered_people';

    //
    public function event()
    {
        return $this->belongsTo('\App\Event');
    }
}
