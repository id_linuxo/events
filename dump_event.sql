-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: event
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_event_template_id` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_form_event_template_id_foreign` (`form_event_template_id`),
  CONSTRAINT `events_form_event_template_id_foreign` FOREIGN KEY (`form_event_template_id`) REFERENCES `form_event_templates` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Test','Test description','2018-01-02 10:08:27',1,1,'2018-01-02 09:08:27',NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_event_templates`
--

DROP TABLE IF EXISTS `form_event_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_event_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ser_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_event_templates`
--

LOCK TABLES `form_event_templates` WRITE;
/*!40000 ALTER TABLE `form_event_templates` DISABLE KEYS */;
INSERT INTO `form_event_templates` VALUES (1,'test template','a:32:{s:4:\"name\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:4:\"Nome\";s:8:\"en_label\";s:4:\"Name\";s:9:\"mandatory\";i:1;}s:14:\"spiritual_name\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:15:\"Nome spirituale\";s:8:\"en_label\";s:14:\"Spiritual name\";}s:7:\"surname\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:7:\"Cognome\";s:8:\"en_label\";s:7:\"Surname\";s:9:\"mandatory\";i:1;}s:3:\"sex\";a:5:{s:4:\"type\";s:5:\"radio\";s:8:\"it_label\";s:5:\"Sesso\";s:8:\"en_label\";s:3:\"Sex\";s:9:\"mandatory\";i:1;s:6:\"values\";a:2:{s:6:\"Female\";s:1:\"F\";s:4:\"Male\";s:1:\"M\";}}s:5:\"email\";a:4:{s:4:\"type\";s:5:\"email\";s:8:\"it_label\";s:5:\"Email\";s:8:\"en_label\";s:5:\"Email\";s:9:\"mandatory\";i:1;}s:9:\"birthdate\";a:4:{s:4:\"type\";s:4:\"date\";s:8:\"it_label\";s:15:\"Data di nascita\";s:8:\"en_label\";s:8:\"Birthday\";s:9:\"mandatory\";i:1;}s:10:\"birthplace\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:16:\"Luogo di nascita\";s:8:\"en_label\";s:11:\"Birth place\";s:9:\"mandatory\";i:1;}s:11:\"nationality\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:18:\"Nazionalit&agrave;\";s:8:\"en_label\";s:11:\"Nationality\";s:9:\"mandatory\";i:1;}s:7:\"address\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:9:\"Indirizzo\";s:8:\"en_label\";s:7:\"Address\";s:9:\"mandatory\";i:1;}s:4:\"city\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:12:\"Citt&agrave;\";s:8:\"en_label\";s:4:\"City\";s:9:\"mandatory\";i:1;}s:8:\"province\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:9:\"Provincia\";s:8:\"en_label\";s:8:\"Province\";s:9:\"mandatory\";i:1;}s:3:\"zip\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:3:\"CAP\";s:8:\"en_label\";s:8:\"Zip code\";s:9:\"mandatory\";i:1;}s:6:\"nation\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:20:\"Nazione di residenza\";s:8:\"en_label\";s:6:\"Nation\";s:9:\"mandatory\";i:1;}s:6:\"mobile\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:9:\"Cellulare\";s:8:\"en_label\";s:6:\"Mobile\";s:9:\"mandatory\";i:1;}s:9:\"telephone\";a:4:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:8:\"Telefono\";s:8:\"en_label\";s:8:\"Telefono\";s:9:\"mandatory\";i:1;}s:11:\"time_arrive\";a:3:{s:4:\"type\";s:8:\"datetime\";s:8:\"it_label\";s:11:\"Data arrivo\";s:8:\"en_label\";s:11:\"Arrive date\";}s:14:\"time_departure\";a:3:{s:4:\"type\";s:8:\"datetime\";s:8:\"it_label\";s:13:\"Data partenza\";s:8:\"en_label\";s:14:\"Departure date\";}s:5:\"meals\";a:3:{s:4:\"type\";s:4:\"text\";s:8:\"it_label\";s:5:\"Pasti\";s:8:\"en_label\";s:5:\"Meals\";}s:15:\"meals_allergies\";a:3:{s:4:\"type\";s:4:\"text\";s:8:\"it_label\";s:26:\"Eventuali allergie al cibo\";s:8:\"en_label\";s:14:\"Food allergies\";}s:13:\"accommodation\";a:3:{s:4:\"type\";s:4:\"text\";s:8:\"it_label\";s:12:\"Sistemazione\";s:8:\"en_label\";s:13:\"Accommodation\";}s:4:\"pets\";a:3:{s:4:\"type\";s:4:\"text\";s:8:\"it_label\";s:18:\"Animali al seguito\";s:8:\"en_label\";s:4:\"Pets\";}s:10:\"setting_up\";a:5:{s:4:\"type\";s:5:\"radio\";s:8:\"it_label\";s:12:\"Allestimento\";s:8:\"en_label\";s:10:\"Setting up\";s:6:\"values\";a:2:{s:2:\"No\";i:0;s:3:\"Yes\";i:1;}s:7:\"default\";i:0;}s:22:\"seva_food_distribution\";a:5:{s:4:\"type\";s:5:\"radio\";s:8:\"it_label\";s:24:\"Seva distribuzione pasti\";s:8:\"en_label\";s:22:\"Food distribution Seva\";s:6:\"values\";a:2:{s:2:\"No\";i:0;s:3:\"Yes\";i:1;}s:7:\"default\";i:0;}s:16:\"seva_wash_dishes\";a:5:{s:4:\"type\";s:5:\"radio\";s:8:\"it_label\";s:20:\"Seva lavaggio piatti\";s:8:\"en_label\";s:17:\"Dish washing Seva\";s:6:\"values\";a:2:{s:2:\"No\";i:0;s:3:\"Yes\";i:1;}s:7:\"default\";i:0;}s:12:\"seva_cleanup\";a:5:{s:4:\"type\";s:5:\"radio\";s:8:\"it_label\";s:12:\"Seva pulizie\";s:8:\"en_label\";s:13:\"Clean up Seva\";s:6:\"values\";a:2:{s:2:\"No\";i:0;s:3:\"Yes\";i:1;}s:7:\"default\";i:0;}s:10:\"other_seva\";a:3:{s:4:\"type\";s:4:\"text\";s:8:\"it_label\";s:10:\"Altro Seva\";s:8:\"en_label\";s:10:\"Other Seva\";}s:11:\"dismantling\";a:5:{s:4:\"type\";s:5:\"radio\";s:8:\"it_label\";s:15:\"Disallestimento\";s:8:\"en_label\";s:11:\"Dismantling\";s:6:\"values\";a:2:{s:2:\"No\";i:0;s:3:\"Yes\";i:1;}s:7:\"default\";i:0;}s:11:\"car_pooling\";a:3:{s:4:\"type\";s:4:\"text\";s:8:\"it_label\";s:17:\"Condivisione auto\";s:8:\"en_label\";s:11:\"Car pooling\";}s:7:\"child_1\";a:4:{s:4:\"type\";s:5:\"child\";s:8:\"it_label\";s:6:\"Figlio\";s:8:\"en_label\";s:5:\"Child\";s:6:\"values\";a:4:{s:4:\"name\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:4:\"Nome\";s:8:\"en_label\";s:4:\"Name\";}s:7:\"surname\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:7:\"Cognome\";s:8:\"en_label\";s:7:\"surname\";}s:9:\"birthdate\";a:3:{s:4:\"type\";s:4:\"date\";s:8:\"it_label\";s:15:\"Data di nascita\";s:8:\"en_label\";s:8:\"Birthday\";}s:10:\"birthplace\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:16:\"Luogo di nascita\";s:8:\"en_label\";s:11:\"Birth place\";}}}s:7:\"child_2\";a:4:{s:4:\"type\";s:5:\"child\";s:8:\"it_label\";s:6:\"Figlio\";s:8:\"en_label\";s:5:\"Child\";s:6:\"values\";a:4:{s:4:\"name\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:4:\"Nome\";s:8:\"en_label\";s:4:\"Name\";}s:7:\"surname\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:7:\"Cognome\";s:8:\"en_label\";s:7:\"surname\";}s:9:\"birthdate\";a:3:{s:4:\"type\";s:4:\"date\";s:8:\"it_label\";s:15:\"Data di nascita\";s:8:\"en_label\";s:8:\"Birthday\";}s:10:\"birthplace\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:16:\"Luogo di nascita\";s:8:\"en_label\";s:11:\"Birth place\";}}}s:7:\"child_3\";a:4:{s:4:\"type\";s:5:\"child\";s:8:\"it_label\";s:6:\"Figlio\";s:8:\"en_label\";s:5:\"Child\";s:6:\"values\";a:4:{s:4:\"name\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:4:\"Nome\";s:8:\"en_label\";s:4:\"Name\";}s:7:\"surname\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:7:\"Cognome\";s:8:\"en_label\";s:7:\"surname\";}s:9:\"birthdate\";a:3:{s:4:\"type\";s:4:\"date\";s:8:\"it_label\";s:15:\"Data di nascita\";s:8:\"en_label\";s:8:\"Birthday\";}s:10:\"birthplace\";a:3:{s:4:\"type\";s:6:\"string\";s:8:\"it_label\";s:16:\"Luogo di nascita\";s:8:\"en_label\";s:11:\"Birth place\";}}}s:7:\"privacy\";a:5:{s:4:\"type\";s:8:\"checkbox\";s:8:\"it_label\";s:7:\"Privacy\";s:8:\"en_label\";s:7:\"Privacy\";s:6:\"values\";a:1:{s:7:\"privacy\";i:1;}s:9:\"mandatory\";i:1;}}',1,'2018-01-03 15:28:30','2018-01-03 15:28:30');
/*!40000 ALTER TABLE `form_event_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(6,'2017_12_23_150000_create_form_event_templates_table',2),(7,'2017_12_23_150842_create_events_table',2),(10,'2017_12_23_161635_create_registered_people_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registered_people`
--

DROP TABLE IF EXISTS `registered_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registered_people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spiritual_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `birthplace` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime_arrive` datetime DEFAULT NULL,
  `datetime_departure` datetime DEFAULT NULL,
  `pets` tinyint(4) DEFAULT NULL,
  `ser_other_params` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registered_people_event_id_foreign` (`event_id`),
  CONSTRAINT `registered_people_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registered_people`
--

LOCK TABLES `registered_people` WRITE;
/*!40000 ALTER TABLE `registered_people` DISABLE KEYS */;
INSERT INTO `registered_people` VALUES (1,1,'lino',NULL,'mazza','M','lkjlkjlkj@lkj.it','2018-01-26','kjh','kjh','kjh','kjh','kjh','kjh','kjhkjh','kjhkjh','kjhkjh','2018-01-17 00:00:00','2018-01-25 00:00:00',NULL,NULL,1,'2018-01-02 12:47:35','2018-01-02 12:47:35');
/*!40000 ALTER TABLE `registered_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Lino','linuzzin@gmail.com','$2y$10$1PeWAgFSeWts7ZFKuR.JH.A76DykYNLbRcbqR5vH58N2tdc3khCZy','WHFzqp571EHKJPfOqt2HCcJLCa4qAlupbq9ZwPrc54QoCzKBSTbVjHbwNjgx','2017-12-22 16:10:19','2017-12-22 16:10:19');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-04 16:39:43
