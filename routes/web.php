<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//WELCOME
Route::get('/', function () {
    return view('welcome');
});

//EVENTS

Route::group(['prefix' => 'event', 'namespace' => 'Event'], function() {

    Route::get('/', 'IndexController@index');
    Route::get('/show/{id?}', ['uses' => 'IndexController@index'])->name('event_show');
    Route::post('/save/{id?}', ['uses' => 'IndexController@save'])->name('event_save_registration');
    Route::get('/success', ['uses' => 'IndexController@success'])->name('event_save_registration_success');
    Route::get('/success_1', ['uses' => 'IndexController@success'])->name('event_save_1_registration_success');
    Route::get('/success_2', ['uses' => 'IndexController@success_2'])->name('event_save_2_registration_success');
    Route::get('/success_3', ['uses' => 'IndexController@success_3'])->name('event_save_3_registration_success');
    Route::get('/success_4', ['uses' => 'IndexController@success_4'])->name('event_save_4_registration_success');
    Route::get('/success_5', ['uses' => 'IndexController@success_5'])->name('event_save_5_registration_success');
    Route::get('/success_6', ['uses' => 'IndexController@success_6'])->name('event_save_6_registration_success');
    Route::get('/success_7', ['uses' => 'IndexController@success_7'])->name('event_save_7_registration_success');
    Route::get('/success_8', ['uses' => 'IndexController@success_8'])->name('event_save_8_registration_success');
    Route::get('/success_8_en', ['uses' => 'IndexController@success_8_en'])->name('event_save_8_registration_en_success');
    Route::get('/success_9', ['uses' => 'IndexController@success_9'])->name('event_save_9_registration_success');
    Route::get('/success_10', ['uses' => 'IndexController@success_10'])->name('event_save_10_registration_success');
    Route::get('/success_11', ['uses' => 'IndexController@success_11'])->name('event_save_11_registration_success');
    Route::get('/success_12', ['uses' => 'IndexController@success_12'])->name('event_save_12_registration_success');

});
    
//AUTH 
Auth::routes();

//ADMIN
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {

    Route::get('/', 'IndexController@index')->name('home');
    Route::get('/event/{id?}', ['uses' => 'EventController@index'])->name('event_admin');
    Route::get('/event/settings/{id?}', ['uses' => 'EventController@settings'])->name('event_settings_admin');
    Route::get('/fetcreate/{id?}/{id_template?}', 'FormEventTemplateController@createDefault');
    Route::get('/testsettings/{id?}', 'EventController@save_settings');

});

//AJAX
Route::group(['prefix' => 'ajax', 'namespace' => 'Ajax'], function() {
    Route::post('/get_csv', 'IndexController@getCsv')->name('ajax_get_csv');
    Route::post('/view_csv', 'IndexController@viewCsv')->name('ajax_view_csv');
    Route::post('/enable_disable_event', 'IndexController@eventEnableDisable')->name('ajax_enable_disable_event');
    Route::post('/hide_show_event', 'IndexController@eventHideShow')->name('ajax_hide_show_from_sidebar_event');
    Route::post('/event_settings_save', ['uses' => 'IndexController@save_event_settings'])->name('ajax_save_event_settings');
    Route::post('/delete_rp_from_date', ['uses' => 'IndexController@delete_rp_from_date'])->name('ajax_delete_rp_from_date');

});
