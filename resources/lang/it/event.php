
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'Accetto il regolamento' => 'Accetto il regolamento',
	'head_module' => 'Ti chiediamo di compilare con attenzione e precisione il modulo seguente. Sulla base delle tue indicazioni verranno organizzati<br> per te i pasti e l\'accoglienza. Ti chiediamo di rispettare quanto ci indichi così da poter facilitare il servizio dei volontari.<br> La tua collaborazione è per noi molto preziosa. Grazie!',
	/*'head_module' => 'Con questo modulo ti chiediamo alcune informazioni sul tuo soggiorno al Centro.<br><br> E\' importante che ci indichi con precisione: 
	<ul>
     <li>quando arrivi e quando parti</li>
     <li>se vuoi iscriverti allla serata dell\'ultimo dell\'anno</li>
	 <li>se vuoi ricevere il sankalpa (risoluzione) della puja per cui è richiesto il contributo.</li> 
	</ul>
	Se non vuoi richiedere il sankalpa specifico puoi partecipare liberamente alla puja come spettatore (senza selezionare il sankalpa).',
	 */
	'privacy' => 'privacy',
	'Invia registrazione' => 'Invia registrazione',
	'Iam course yet frequented' => '<b>Dichiaro di aver già appreso la tecnica ad un corso IAM. *</b>',
	'EM course' => '<b>Mi iscrivo al corso EM.</b>',
	'Get Growing course' => '<b>Mi iscrivo ai tre corsi Get Growing</b>',
    'by car' => 'Arriverai in AUTO',
    'by train' => 'Arriverai in TRENO',


];
