
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'Accetto il regolamento' => 'I accept the Regulation',
	'Compilare il seguente modulo di iscrizione (i campi contrassegnati con * sono obbligatori)' => 'Fill in the following registration form (fields marked * are required)',
	'privacy' => 'privacy',
	'Invia registrazione' => 'Send registration',

];
