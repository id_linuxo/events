@extends('event.partials.event_template')
@section('content')
<div align="center">

<p>&nbsp;</p>

<p><h1>We have received your registration and we thank you very much !</h1></p>

<p><h2>After making the donation, please send the screenshot of the payment by email within 5 days to the following address: 9000kmdamore@gmail.com.</h2></p>

<p><h3>Photos  or video of your walk are very welcome. Good  path!</h3></p>

<p><h3>Thank you!</h3></p>

<p>The Team of Amma Italia</p>

</div>
@endsection
