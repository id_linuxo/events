@extends('event.partials.event_template')
@section('content')
<div align="center">

<p><h1>Abbiamo ricevuto la tua richiesta di adesione e ti ringraziamo di cuore.</h1></p>

<p><h2>Dopo aver effettuato la donazione, ti preghiamo di inviare entro 5 giorni <br>la ricevuta o screenshot del versamento via mail al seguente indirizzo:&nbsp;<br><strong>9000kmdamore@gmail.com</strong></h2></p>
</div>
<br/>
<br/>
<center>
<h3>
Clicca sul bottone DONA ORA per effettuare la tua donazione, verrai indirizzato alla piattaforma "Buonacausa"<br> 
dove troverai le diverse modalità con cui donare e dove potrai lasciare (se vorrai) una tua frase, un commento, o un aforisma che tutti potranno leggere.<br>
Ti ricordiamo che se vuoi fare una donazione maggiore, puoi selezionare "offerta libera" indicando l'importo desiderato.
</h3>
</center>
<a href="https://buonacausa.org/cause/9000kmdamore2/donate" target="_blank">
<div align="center" style="font-size:30px;color:#fff;text-align:center;background-color: #8f8fbc;height:50px">
  <span style="vertical-align:middle"><strong>DONA ORA</strong></span>
</div>
</a>
<br/>
<br/>
<div align="center">
<h3>Sono molto gradite foto o video della tua camminata. Buon percorso!</h3>

<p><h3>Grazie!</3></p>
<p>Il Team Amma Italia </p>
</div>
@endsection
