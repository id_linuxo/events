@extends('event.partials.event_template')
@section('content')
<div align="center">

<p>&nbsp;</p>
<p>Aum  Amriteswaryai Namaha</p>

<p>La tua registrazione on. line, è avvenuta con successo!</p>

<p>Controlla la tua posta anche nella spam!</p>

<p>A breve riceverai la e.mail che conferma la tua iscrizione, con tutte le informazioni utili della giornata.</p>

<p>Se non ricevi nessuna e,mail <b>attivati e contatta</b> la persona di riferimento all'evento.

<p>Ti invitiamo a controllare di frequente la tua casella di posta e di salvare tra i tuoi contatti il nostro indirizzo:</p>
<p>@if(!empty($email)){{$email}} @else oneday.ammaitalia@gmail.com @endif</p>
<p>Grazie!</p>
<p>Il Team Amma Italia </p>
</div>
@endsection
