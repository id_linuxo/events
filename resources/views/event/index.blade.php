@extends('event.partials.event_template')
@section('content')
  <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{!! __('event.head_module') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="{{ route('event_save_registration') }}/{{ $id }}">
				<input type="hidden" name="event_id" value="{{ $id }}">
            {{ csrf_field() }}
              <div class="box-body">
            @foreach($form_event_fields as $k => $v)
               @php 
                $required = '';
                if(!empty($v['mandatory'])){
                    $required = 'required';
                }
                switch($v['type']){
                    case 'string':
						$txtparams = '';
						if(!empty($v['params'])){
							foreach($v['params'] as $kp => $vp){
								$txtparams .= $kp.'='.$vp.' ';
							}
						}
                        @endphp
                        <div class="form-group">
                        <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                        <div class="col-sm-10">
                        <input type="text" value="{{ old($k) }}" {{$txtparams}} class="form-control"  name="{{$k}}" id="{{$k}}" placeholder="{!! $v[$locale.'_label'] !!}..." {{ $required }}>
                        </div>
                        </div>
                        @php
                    break;
					case 'number':
						$txtparams = '';
						if(!empty($v['params'])){
							foreach($v['params'] as $kp => $vp){
								$txtparams .= $kp.'='.$vp.' ';
							}
						}
                        @endphp
                        <div class="form-group">
                        <label class="col-sm-1 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                        <div class="col-sm-1">
                        <input type="number" min="0" max="150" step="1" value="{{ old($k) }}" {{$txtparams}} class="form-control"  name="{{$k}}" id="{{$k}}" placeholder="{!! $v[$locale.'_label'] !!}..." {{ $required }}>
                        </div>
                        </div>
                        @php
					break;
                    case 'email':
                        @endphp
                        <div class="form-group">
                        <label class="col-sm-10 control-label">{!!$v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                        <div class="col-sm-10">
                        <input type="email" value="{{ old($k) }}" class="form-control"  name="{{$k}}" id="{{$k}}" placeholder="{{ $k }}..." {{ $required }}>
                        </div>
                        </div>
                        @php
                    break;
                    case 'text':
						$txtparams = 'rows=3';
						if(!empty($v['params'])){
							$txtparams = '';
							foreach($v['params'] as $kp => $vp){
								$txtparams .= $kp.'='.$vp.' ';
							}
						}
                        @endphp
                        <div class="form-group">
                        <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                        @if(!empty($v[$locale.'_sub_label']))
                           <label class="col-sm-10 control-label"><i>{!! $v[$locale.'_sub_label'] !!}</i></label>
                        @endif
                        <textarea name="{{$k}}" id="{{$k}}" {{$txtparams}} class="form-control" placeholder="" {{ $required }}>{{ old($k) }}</textarea>
                        <div class="col-sm-10">
                        </div>
                        </div>
                        @php
                    break;
                    case 'date':
                    case 'datetime':
                        @endphp
                        <!-- Date -->
                        <div class="form-group">
                            <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" value="{{ old($k) }}" class="form-control pull-right" id="datepicker_{{ $k }}" name="{{ $k }}" {{ $required }} readonly>
                                </div>
                            </div>
                        <!-- /.input group -->
                        </div>
                        @php
                    break;
                    /*
                    case 'datetime':
                        @endphp
                        <!-- Date -->
                        <div class="form-group">
                        <label class="col-sm-2 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label>
                        <div class="col-sm-10">
                        <div class="input-group datetime">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" value="{{ old($k) }}" class="form-control pull-right" id="datetimepicker_{{ $k }}" name="{{ $k }}" {{ $required }} value="">
                        </div>
                        </div>
                        <!-- /.input group -->
                        </div>
                        @php
                    break;
                    */
                    case 'radio':
                          @endphp
                          <div style="clear:both"></div>
                          @php
                          $label_class = "col-sm-2 control-label";
                          $div_class = "col-sm-10";
						  $onClick = !empty($v['onClick']) ? "onclick = \"{$v['onClick']}\"" : '';
                          if(!empty($v[$locale.'_sub_label'])) {
                              @endphp
                                <div class="form-group">
                                <label class="col-sm-2 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                                <label class="col-sm-10 control-label"><i>{!! $v[$locale.'_sub_label'] !!}</i>&nbsp;&nbsp;
                                @foreach($v['values'] as $kv => $vv) 
                                    @php
                                        $checked = '';
                                    @endphp
                                    @if(isset($v['default'])) 
                                        @if(($v['default'] == $vv) || (!empty(old($k)) && ($vv == old($k))))
                                            @php
                                                $checked = 'checked';
                                            @endphp
                                        @endif
                                    @endif
                                      <input {!! $onClick !!} type="radio" name="{{ $k }}" id="{{ $k }}optionsRadio{{ $kv }}" value="{{ $vv }}" {{ $checked }} {{ $required }}>&nbsp;{{ $kv }} 
                                @endforeach
                                </label>
                                </div>
                              @php
                          } else {
                              if(strlen($v[$locale.'_label']) > 10){
                                  $label_class = "col-sm-5 control-label";
                                  $div_class = "col-sm-2";
                              }
                              @endphp
                                <div class="form-group">
                                <label class={{$label_class}}>{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label>
                                <div class={{$div_class}}>
                                @foreach($v['values'] as $kv => $vv) 
                                    @php
                                        $checked = '';
                                    @endphp
                                    @if(!empty($v['default']) && empty(old($k))) 
                                        @if($v['default'] == $vv)
                                            @php
                                                $checked = 'checked';
                                            @endphp
                                        @endif
                                    @endif
                                    @if(!empty(old($k)) && ($vv == old($k)))
                                        @php
                                            $checked = 'checked';
                                        @endphp
                                    @endif

                                      <input {!! $onClick !!} type="radio" name="{{ $k }}" id="{{ $k }}optionsRadio{{ $kv }}" value="{{ $vv }}" {{ $checked }} {{ $required }}>&nbsp;{{ $kv }} 
                                @endforeach
                                </div>
                                </div>
                                @php
                          }
                    break;
                    case 'checkbox':
                          @endphp
                            <div class="form-group">
                            <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!}</label>
                            <div class="col-sm-10">
                            @foreach($v['values'] as $kv => $vv) 
                                @php
                                    $checked = '';
                                @endphp
                                @if(isset($v['default'])) 
                                    @if(($v['default'] == $vv) || (!empty(old($k)) && ($vv == old($k))))
                                        @php
                                            $checked = 'checked';
                                        @endphp
                                    @endif
                                @elseif(!empty(old($k)) && ($vv == old($k)))
                                    @php
                                    $checked = 'checked';
                                    @endphp
                                @endif
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" name="{{ $k }}" id="{{ $k }}optionsCheckbox{{ $kv }}" value="{{ $vv }}" {{ $checked }} {{ $required }} @php if(!empty($v['oninvalid'])){ @endphp oninvalid="this.setCustomValidity('{!! $v['oninvalid'] !!}')" onchange="this.setCustomValidity('')" @php } @endphp>
                                  {!!  __('event.'.$kv) !!} 
                                </label>
                              </div>
                            @endforeach
                            </div>
                            </div>
                          @php
                    break;
                    case 'child':
                         @endphp
                    <div class="form-group">
                        <label>{!! $v[$locale.'_label'] !!}</label></br> 
                    @foreach($v['values'] as $kv => $vv)
                        @php
                        $child_required = '';
                        if(!empty($vv['mandatory'])){
                            $child_required = 'required';
                        }
                        switch($vv['type']){
							case 'radio':
							@endphp
                            <div class="form-group">
							{!! $vv[$locale.'_label'] !!}&nbsp;&nbsp;&nbsp;			
                                @foreach($vv['values'] as $kvv => $vvv) 
                                    @php
                                        $checked = '';
                                    @endphp
                                    @if(isset($vv['default'])) 
                                        @if(($vv['default'] == $vvv) || (!empty(old($kv)) && ($vvv == old($kv))))
                                            @php
                                                $checked = 'checked';
                                            @endphp
                                        @endif
                                    @endif
                                      <input {!! $onClick !!} type="radio" name="{{ $k }}_{{$kv}}" id="{{ $kv }}optionsRadio{{ $kv }}" value="{{ $vvv }}" {{ $checked }} {{ $required }}>&nbsp;{{ $kvv }} 
                                @endforeach
							</div>
							@php
							break;
                            case 'string':
                            @endphp
                            {!! $vv[$locale.'_label'] !!} <input type="text" value="{{ old($k.'_'.$kv) }}" class="form-control"  name="{{ $k }}_{{ $kv }}" id="{{ $k }}_{{ $kv }}" placeholder="{{ strip_tags($kv) }}..." {{ $child_required }}>
                            @php
                            break;
							case 'number':
							@endphp
							{!! $vv[$locale.'_label'] !!} <input type="number" style="width: 5em;" min="0" max="150" step="1" value="{{ old($k.'_'.$kv) }}" {{$txtparams}} class="form-control"  name="{{$k}}_{{ $kv }}" id="{{$k}}" placeholder="{{ strip_tags($kv) }}..." {{ $required }}>
							@php
							break;
                            case 'date':
                            @endphp
                            {!! $vv[$locale.'_label'] !!}
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" value="{{ old($k.'_'.$kv) }}" class="form-control pull-right" id="datepicker_{{ $k }}_{{ $kv }}" name="{{ $k }}_{{ $kv }}" {{ $child_required }}>
                            </div>
                            @php
                            break;
                            case 'text':
                                @endphp
                                <div class="form-group">
                                <label>{!! $vv[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                                @if(!empty($vv[$locale.'_sub_label']))
                                   <label class="col-sm-10 control-label"><i>{!! $vv[$locale.'_sub_label'] !!}</i></label>
                                @endif
                                <textarea name="{{$k}}_{{$kv}}" id="{{$k}}_{{$kv}}" class="form-control" rows="3" placeholder="" {{ $child_required }}>{{ old($k.'_'.$kv) }}</textarea>
                                <div class="col-sm-10">
                                </div>
                                </div>
                                @php
                            break;
                        }
                        @endphp
                    @endforeach
                    </div>
                        @php
                    break;
                    case 'family':
                         @endphp
                    <div class="form-group">
                        <label>{!! $v[$locale.'_label'] !!}</label> 
                    @foreach($v['values'] as $kv => $vv)
                        @php
                        $family_required = '';
                        if(!empty($vv['mandatory'])){
                            $family_required = 'required';
                        }
                        switch($vv['type']){
                            case 'string':
                            @endphp
                            <div align="left">{!! $vv[$locale.'_label'] !!}</div> <input type="text" value="{{ old($k.'_'.$kv) }}" class="form-control"  name="{{ $k }}_{{ $kv }}" id="{{ $k }}_{{ $kv }}" placeholder="{{ $kv }}..." {{ $family_required }}>
                            @php
                            break;
                            case 'date':
                            @endphp
                            {!! $vv[$locale.'_label'] !!}
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" value="{{ old($k.'_'.$kv) }}" class="form-control pull-right" id="datepicker_{{ $k }}_{{ $kv }}" name="{{ $k }}_{{ $kv }}" {{ $family_required }}>
                            </div>
                            @php
                            break;
                            case 'text':
                                @endphp
                                <div class="form-group">
                                <label>{!! $vv[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                                @if(!empty($vv[$locale.'_sub_label']))
                                   <label class="col-sm-10 control-label"><i>{!! $vv[$locale.'_sub_label'] !!}</i></label>
                                @endif
                                <textarea name="{{$k}}_{{$kv}}" id="{{$k}}_{{$kv}}" class="form-control" rows="3" placeholder="{{ $kv }}..." {{ $family_required }}>{{ old($k.'_'.$kv) }}</textarea>
                                <div class="col-sm-10">
                                </div>
                                </div>
                                @php
                            break;
							case 'radio':
								  @endphp
								  <!--div style="clear:both"></div-->
								  </br>
								  @php
								  $label_class = "col-sm-2 control-label";
								  $div_class = "col-sm-10";
								  if(!empty($vv[$locale.'_sub_label'])) {
									  @endphp
										<div class="form-group">
										<label class="col-sm-2 control-label">{!! $vv[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
										<label class="col-sm-10 control-label"><i>{!! $vv[$locale.'_sub_label'] !!}</i>&nbsp;&nbsp;
										@foreach($vv['values'] as $kvv => $vvv) 
											@php
												$checked = '';
											    $family_radio_key = $k.'_'.$kv;	
											@endphp
											@if(isset($vvv['default'])) 
												@if(($vvv['default'] == $vvv) || (!empty(old($family_radio_key)) && ($vvv == old($family_radio_key))))
													@php
														$checked = 'checked';
													@endphp
												@endif
											@endif
											  <input type="radio" name="{{ $k }}_{{ $kv }}" id="{{ $k }}optionsRadio{{ $kvv }}" value="{{ $vvv }}" {{ $checked }} {{ $required }}>&nbsp;{{ $kvv }} 
										@endforeach
										</label>
										</div>
									  @php
								  } else {
									  if(strlen($vv[$locale.'_label']) > 10){
										  $label_class = "col-sm-2 control-label";
										  $div_class = "col-sm-2";
									  }
									  @endphp
										<!-- div class="form-group" -->
										<label class={{$label_class}}>{!! $vv[$locale.'_label'] !!} @if(!empty($required)) * @endif</label>
										<div class={{$div_class}}>
										@foreach($vv['values'] as $kvv => $vvv) 
											@php
												$checked = '';
											    $family_radio_key = $k.'_'.$kv;	
											@endphp
											@if(!empty($vvv['default']) && empty(old($family_radio_key))) 
												@if($vvv['default'] == $vvv)
													@php
														$checked = 'checked';
													@endphp
												@endif
											@endif
											@if(!empty(old($family_radio_key)) && ($vvv == old($family_radio_key)))
												@php
													$checked = 'checked';
												@endphp
											@endif

											  <input type="radio" name="{{ $k }}_{{ $kv}}" id="{{ $k }}optionsRadio{{ $kvv }}" value="{{ $vvv }}" {{ $checked }} {{ $required }}>&nbsp;{{ $kvv }} 
										@endforeach
										</div>
										<!--/div-->
										@php
								  }
							break;
                        }
                        @endphp
                    @endforeach
                    </div>
                        @php
                    break;
                    case 'row':
                    @endphp
                    <div class="form-group">
                        <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!}</label> 
                        @if(!empty($v[$locale.'_sub_label']))
                           <label class="col-sm-10 control-label"><i>{!! $v[$locale.'_sub_label'] !!}</i></label>
                        @endif
                    </div>
                    @php
                    break;
                    case 'select_week':
					$days_of_week = Array(
										'domenica' => 'Domenica/Sunday',
									    'lunedi' => 'Luned&igrave/Monday', 
										'martedi' => 'Marted&igrave/Tuesday', 
										'mercoledi' => 'Mercoled&igrave/Wednesday', 
										'giovedi' => 'Gioved&igrave/Thursday', 
										'venerdi' => 'Venerd&igrave/Friday',
										'sabato' => 'Sabato/Saturday',
									);

					
					if(!empty($v['values'])){
						$tmp_days_of_week = [];
						foreach($v['values'] as $vd){
							if(array_key_exists($vd, $days_of_week)){
								$tmp_days_of_week[$vd] = $days_of_week[$vd];
							} 
						}
						$days_of_week = $tmp_days_of_week;
					}
					
					if(!empty($page_ser_other_params[$k])){
						$tmp_days_of_week = [];
						foreach($page_ser_other_params[$k] as $kd => $vd){
							$tmp_days_of_week[$kd] = $vd;
						}
						$days_of_week = $tmp_days_of_week;
					}
                    @endphp
                    <div class="form-group">
                        <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                            <div class="col-sm-5">
								<select class="form-control" name="{{$k}}" id="{{$k}}" {{ $required }}>
									<option value="">---</option>
									@foreach($days_of_week as $dk => $dv)
								    @php $selected = ''; @endphp	
									@if(!empty(old($k)) && (old($k) == $dk))
                                        @php
                                            $selected = 'selected';
                                        @endphp
									@endif
									<option value="{{$dk}}" {{$selected}}>{!! $dv !!}</option>
									@endforeach
								</select>
							</div>
                    </div>
                    @php
					break;
					case 'select_time':
                    @endphp
                    <div class="form-group">
                        <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                            <div class="col-sm-3">
								<select class="form-control" name="{{$k}}" id="{{$k}}" {{ $required }}>
									<option value="">---</option>
                        			@if(!empty($v['values']))
	                                	@foreach($v['values'] as $kv => $vv) 
								    	@php $selected = ''; @endphp	
										@if(!empty(old($k)) && (old($k) == $vv))
                                        @php
                                            $selected = 'selected';
                                        @endphp
										@endif
										<option value="{{$vv}}" {{$selected}}>{{$vv}}</option>
										@endforeach
									@endif
								</select>
							</div>
                    </div>
                    @php
					break;
					case 'select_text':
						$hide = '';
						if(!empty($v['hide'])){
							$hide = 'style="display:none"';
						}
                    @endphp
                    <div class="form-group" {!! $hide !!}>
                        <label class="col-sm-10 control-label">{!! $v[$locale.'_label'] !!} @if(!empty($required)) * @endif</label> 
                            <div class="col-sm-5">
								<select class="form-control" name="{{$k}}" id="{{$k}}" {{ $required }}>
									<option value="">---</option>
                        			@if(!empty($v['values']))
	                                	@foreach($v['values'] as $kv => $vv) 
								    	@php $selected = ''; @endphp	
										@if(!empty(old($k)) && (old($k) == $vv))
                                        @php
                                            $selected = 'selected';
                                        @endphp
										@endif
										<option value="{{$vv}}" {{$selected}}>{{$vv}}</option>
										@endforeach
									@endif
								</select>
							</div>
                    </div>
                    @php
					break;
					case 'button_area':
					$style = '';
					if(!empty($v['style'])){
						$style = $v['style']; 
					}
					@endphp
                    <div class="form-group">
						@php 
							if(!empty($v['href'])){
								$target = (!empty($v['blank']) ?  '_blank' : '_self');
						@endphp
						<a href="{!! $v['href'] !!}" target="{{$target}}">
						 <div class="col-sm-5" style="{{$style}}">
							<span>{!! $v[$locale.'_label'] !!}</span>
						 </div>
						</a>
						@php
							} else {
						@endphp
						 <div class="col-sm-5" style="{{$style}}">
							<span>{!!$v[$locale.'_label'] !!}</span>
						 </div>
						@php
							}
						@endphp
					</div>
					@php
					break;
                    default:
                    break;
                }
                @endphp
                <!-- empty row -->
                <div>&nbsp;</div>
            @endforeach
              </div>
            <div class="form-group">
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{ __('event.'.'Invia registrazione') }}</button>
              </div>
            </div>
			<input type="hidden" name="locale" id="locale" value="{{ $locale }}">
            </form>
          </div>
        </div>
      </div>
   </section>
@endsection
