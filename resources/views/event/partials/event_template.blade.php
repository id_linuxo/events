<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Amma Italia - Eventi </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-purple.min.css">
  
  <!-- daterange picker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="skin-purple sidebar-collapse">
<div class="wrapper">
@include('event.partials.header');
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
			<div class="col-md-10">
                {!! $page_title !!}
                &nbsp;&nbsp;&nbsp;&nbsp;<small><b><u>{!! $page_description !!}</u></b></small>&nbsp;&nbsp;
                {{ $page_motto }}
                @if(!empty($page_image))
                <img src="/{{ $page_image }}" align="right" style="padding-bottom:10px">
                @endif
				@if(!empty($display))
					<style>
					.blink_me {
					  animation: blinker 1s linear infinite;
					  color:red;
					}

					@keyframes blinker {
					  50% {
						opacity: 0;
					  }
					}
					.buttona {
					  font: bold 18px Arial;
					  text-decoration: none;
					  background-color: #EEEEEE;
					  color: #333333;
					  padding: 2px 6px 2px 6px;
					  border-top: 1px solid #CCCCCC;
					  border-right: 1px solid #333333;
					  border-bottom: 1px solid #333333;
					  border-left: 1px solid #CCCCCC;
					}
					</style>
					&nbsp;<span class="blink_me">PREVIEW MODULO</span>
					<a class="buttona" href="{{route('event_admin')}}/{{$id}}">TORNA AL PANNELLO</a>
				@endif
				@php
					if(App::getLocale() == 'it'){
				@endphp
						&nbsp;&nbsp;&nbsp;<!-- a href="?locale=en"><u>(Click here for English version)</u></a -->
				@php
					} else { 
				 @endphp
						&nbsp;&nbsp;&nbsp;<a href="?locale=it"><u>(Versione in italiano)</u></a>
				@php
					}  
				 @endphp
			</div>
			</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <!--ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol-->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
@include('event.partials.footer')

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.3 -->
<script src="{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/admin-lte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
<!-- date-range-picker -->
<script src="/bower_components/moment/min/moment.min.js"></script>
<!--script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script-->
<!-- bootstrap datepicker -->
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.it.min.js"></script>
<script>
 //Date picker
    $('[id^="datepicker"]').datepicker({
	  language: 'it',
      format: 'dd-mm-yyyy',
      autoclose: false,
      defaultDate: '01-01-1970'
    });
/*
    $('[id^="datetimepicker"]').daterangepicker({
        autoclose: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        opens: 'center',
        format: 'DD-MM-YYYY HH:mm'
    });
*/
	function chooseonlythis(radio) {
		if(radio.value == 0){
			return false;
		}
		for(i=1;i<=5;i++){
			if(radio.name != "event_"+i){
				if($("#event_"+i+"optionsRadioNo").length){
					$("#event_"+i+"optionsRadioNo").prop("checked", true);
				}
			}
		}
	}
</script>
</body>
</html>
