<html>
<head></head>
<body>

<h1>Abbiamo ricevuto la tua richiesta di adesione e ti ringraziamo di cuore.</h1>
<p>
Se non l'hai gi&agrave; fatto nel modulo, puoi
<br>
cliccare sul bottone DONA ORA per effettuare la tua donazione. 
Verrai indirizzato alla piattaforma "Buonacausa" dove troverai le diverse modalit&agrave; con cui donare e dove potrai lasciare (se vorrai) 
una tua frase, un commento o un aforisma che tutti potranno leggere.
</p>

<a href="https://buonacausa.org/cause/9000kmdamore2/donate" target="_blank">
<div align="center" style="font-size:30px;color:#fff;text-align:center;background-color: #8f8fbc;height:50px">
  <span style="vertical-align:middle"><strong>DONA ORA</strong></span>
</div>
</a>
<em>(Potrai cliccare su questo pulsante ogni volta che vorrai fare altre piccole donazioni, senza doverti iscrivere di nuovo)</em>

<p>Dopo aver effettuato la donazione, ti preghiamo di inviare entro 5 giorni <br>la ricevuta o screenshot del versamento via mail al seguente indirizzo:&nbsp;<strong>9000kmdamore@gmail.com</strong></p>
<br/>
<br/>
<p><u>Attenzione! La email automatica che ricevi da Buona Causa non &egrave; la ricevuta di pagamento!</u></p>

<br/>
<br/>

<p>Non dimenticarti di inviarci alcune foto e un tuo feedback di come stai percorrendo i tuoi km d'Amore, cos&igrave; da condividere la tua esperienza sulle pagine social dell'evento.</p>
<p>Alla fine dell'evento saranno riconosciuti i percorsi pi&ugrave; originali</p>
<p>Buon percorso!</p>

<p>Grazie!</p>

<p>{{$email_signature}}</p>
<br/>

</body>
</html>
