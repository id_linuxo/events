<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>&nbsp;</p>
<p>Un caro saluto a te,</p>

<p>ti diamo il benvenuto e confermiamo che abbiamo ricevuto al tua iscrizione alla <b>"{{$event_name}}"</b> che si terrà il {{$time_description}} presso il MA Center Italy.</p>

<p>In caso tu  abbia segnalato nel modulo d’iscrizione che <u>non</u> sarai presente fisicamente, nei giorni successivi alla cerimonia riceverai il Prasad (un dono simbolico benedetto durante la Puja).</p>

<p><b>MODALITA' DI PARTECIPAZIONE</b></p>

<p>
Per partecipare alla Puja &egrave; richiesto un contributo di <b>70,00 euro</b> che vale sia per il singolo che per i componenti 

del nucleo famigliare, indicati all'atto dell'iscrizione. (fino ad un numero di 4 persone).

Il contributo sar&agrave; interamente devoluto ai progetti caritatevoli di Embracing the World.
</p>

<p>La donazione può essere effettuata con BONIFICO BANCARIO intestato ad Amma Italia:</p>

<p><b>Banca Etica</b><br>
IBAN: IT62Q0501802600000012435376<br>
codice BIC: CCRTIT2T84A<br></p>

<p><b>Unicredit conto solidariet&agrave;</b><br>
IBAN: IT47E0200811100000002513014<br>
codice BIC Swift: UNCRITM1300<br></p>

<p><b>PayPal</b><br>
<a href="https://www.paypal.com/donate/?hosted_button_id=RRMR7QBEHMY8Q" target="_blank">IBAN: IT47E0200811100000002513014</a>
</p>

<p>
<b>Causale di versamento</b><br>
Ricordati di specificare come causale di versamento: "<u>Erogazione liberale Puja 3 gennaio 2023</u>"</p>

<p>Ti preghiamo di inviare la disposizione di pagamento o la conferma di Paypal all’indirizzo: cerimonie.centro@gmail.com e di portarla con te alla cerimonia.</p>

<p>Per informazioni puoi contattare:  Eugenia Agostini 339 8315400<br>
e-mail: cerimonie.centro@gmail.com</p>

<p>In caso la tua prenotazione subisca variazioni, ti preghiamo di avvertirci in anticipo.</p>

Grazie e a presto.

<p>{{$email_signature}}</p>
</body>
</html>
