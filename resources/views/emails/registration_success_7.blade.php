<html>
<head></head>
<body>

<h1>Abbiamo ricevuto la tua richiesta di adesione.</h1>

<p>A breve riceverai un e.email che conferma la tua iscrizione, con tutte le informazioni necessarie per partecipare.</p>

<p>Controlla la tua casella di posta anche nella Spam!</p>

<p>Questa &egrave; un'email automatica e quindi non &egrave; necessario rispondere.</p>

<p>Grazie!</p>

<p>{{$email_signature}}</p>
</body>
</html>
