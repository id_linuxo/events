<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>&nbsp;</p>
<p>Un caro saluto a te,</p>

<p>Abbiamo ricevuto la tua richiesta di adesione al Corso per il Ripasso della tecnica di meditazione IAM.</p>

<p>Ti ricordiamo che il corso si terrà Sabato 6 Febbraio 2022 alle 9.30 sulla piattaforma zoom</p>

<p>Riceverai i codici di accesso il giorno prima dell'evento, quindi fai attenzione alla posta, anche nello spam!</p>

<p>Qual'ora tu ne fossi sprovvisto, ti consigliamo di scaricare l'applicazione zoom con anticipo e testare la funzionalità
così da essere pronto al momento dell'inizio del corso.</p>

<p>Attenzione!  Questa è un'email automatica quindi non rispondere.</p>

<p>Per necessità puoi contattarci :</p>

<p>iam.iscrizioni@gmail.com</p>
<p>Anna 347 186 1908</p>


<p>Siamo lieti della tua partecipazione!</p>
<p>A presto!</p>

<p>{{$email_signature}}</p>
</body>
</html>
