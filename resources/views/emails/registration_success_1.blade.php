<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>Aum  Amriteswaryai Namaha</p>

<p>A breve riceverai un e.email che conferma la tua iscrizione, con tutte le informazioni utili.</p>

<p>Controlla la tua casella di posta anche nella Spam!</p>

<p><b>Se non ricevi la e.mail di conferma contatta la persona di riferimento all'evento</b></p>

<p>Salva il nostro indirizzo tra i tuoi contatti: {{$mail_to_send}}</p>

<p>Questa &egrave; un'email automatica e quindi non &egrave; necessario rispondere.</p>

<p>Grazie!</p>

<p>{{$email_signature}}</p>
</body>
</html>
