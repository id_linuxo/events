<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>&nbsp;</p>
<p>Un caro saluto a te,</p>

<p>Abbiamo ricevuto la tua richiesta di adesione per i corsi Orto Amico ( Em e Get Growing ).</p>

<p>Per partecipare è richiesta una donazione liberale (che sarà devoluta interamente per i progetti caritatevoli di Embracing The World) 
come ringraziamento per il servizio offerto.</p>

<p>Queste le coordinate per la tua donazione:</p>
<ul>
<li>UniCredit filiale di Bergamo Centro (Conto Solidarietà - potrete inviare il vostro contributo senza nessuna spesa presso tutti gli sportelli Unicredit)  </li>
<li>IBAN: IT47 E 020081 110000000 2513014  codice BIC SWIFT: UNCRITM1300</li>
<li>Poste italiane: IT83 W076 0113 4000 0008 3508 697 codice BIC Swift BPPIITRRXXX</li>
<li>Oppure, tramite bollettino postale: Conto numero 83508697 intestato ad Amma Italia, Matelica (MC)</li>
</ul>

<p><b>IMPORTANTE:</b></p>
<p>
Ti preghiamo di specificare la causale del versamento: “Erogazione liberale corso Orto Amico" e di inviare la ricevuta via mail all'indirizzo: 
oneday.ammaitalia@gmail.com
Riceverai quindi i codici di accesso al corso a cui ti sei iscritto.
I corsi si terranno su piattaforma Zoom: qualora tu ne fossi sprovvisto, 
ti consigliamo di scaricare gratuitamente l'applicazione Zoom con anticipo e testarne la funzionalità, così da essere pronto al momento dell'inizio del corso. </p>

<p>Attenzione!  Questa è un'email automatica quindi non rispondere.</p>

<p>Per necessità puoi contattarci :</p>

<p>oneday.ammaitalia@gmail.com</p>
<p>Emanuela:3456198806</p>


<p>Siamo lieti della tua partecipazione!</p>
<p>A presto!</p>

<p>{{$email_signature}}</p>
</body>
</html>
