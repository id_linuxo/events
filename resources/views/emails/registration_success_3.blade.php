<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>Aum  Amriteswaryai Namaha</p>

<p>Tra qualche giorno riceverai e-mail con i dettagli utili per la tua iscrizione.</p>

<p>Se dopo tale periodo non ricevi alcuna e-mail attivati e scrivi a ritiri.centro@gmail.com</p>

<p><b>Prima però controlla anche nello Spam!</b></p>

<p>Questa &egrave; un'email automatica e quindi non &egrave; necessario rispondere.</p>

<p>Grazie!</p>

<p>{{$email_signature}}</p>
</body>
</html>
