<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>&nbsp;</p>
<p>Un caro saluto a te,</p>

<p>Ti ringraziamo per la tua partecipazione all'<b>inaugurazione del Centro di Amma in Italia</b> e ti invitiamo a cliccare sul link seguente:</p>
<p align="center"><a href="https://www.amma-italia.it/informazioni-per-linaugurazione/">INFORMAZIONI INAUGURAZIONE</a></p>
<p>dove troverai tutte le informazioni sulle strutture esterne, sui parcheggi esterni al centro che ti consigliamo di utilizzare anche se non dormirai al centro, sugli orari dei treni e sul servizio navetta che metteremo a disposizione in diverse fasce orarie</p>

<p><b>Se hai richiesto di pernottare al Centro o ti sei iscritto alla homa riceverai una mail separata con i vari dettagli,</b></p>

<p>Non vediamo l'ora di vederti per questa occasione speciale!</p>
<p>Grazie!</p>

<p>{{$email_signature}}</p>
</body>
</html>
