<html>
<head></head>
<body>

<h1>{{$title}}</h1>

<p>Aum  Amriteswaryai Namaha</p>

<p>Questa mail automatica indica che <strong>abbiamo ricevuto la tua richiesta</strong>.</p>

<p>Nei prossimi giorni  riceverai email di conferma con i dettagli utili per <strong>completare</strong> la tua iscrizione.</p>

<p>Se dopo tale periodo non ricevi alcuna e-mail attivati e chiedi informazioni a info.ritiri.centro@gmail.com</p>

<p><b>Prima però controlla anche nello Spam!</b></p>

<p>Questa &egrave; un'email automatica e quindi non &egrave; necessario rispondere.</p>

<p>Grazie!</p>

<p>{{$email_signature}}</p>
</body>
</html>
