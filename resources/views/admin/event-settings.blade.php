@extends('admin.partials.admin_template')

@section('content')
<section class="content">
<div class="row">
<h1>
Impostazioni per modulo: <strong>{{$event->name}}</strong>
</h1>
</div>
<br>
@if(!empty($arr_settings['events']))
	@php $k = 1; @endphp
	@foreach($arr_settings['events'] as $evento)
		<div class="row">
			<table class="form-group">
				<tr>
                 <td>Label Excel: <input type="text" name="event_label_{{$k}}" id="event_label_{{$k}}" value="{{ $evento['label'] }}"></td>
                 <td>Evento: <input type="text" size="50" name="event_title_{{$k}}" id="event_title_{{$k}}" value="{{ $evento['description'] }}"></td>
                 <td>Data disattivazione: <input type="date" name="event_date_start_{{$k}}" id="event_date_start_{{$k}}"  value="{{ $evento['start'] }}"></td>
                 <td>Alert: <input type="text" size="50" name="event_alert_{{$k}}" id="event_alert_{{$k}}" value="{{ $evento['alert'] }}"><td>
                </tr>
			</table>
		</div>
	@php $k++; @endphp
	@endforeach
@endif
<div class="row">
	<table class="form-group">
		<tr><td>Alert blocco PERNOTTO E CAMERE <input name="alert_accommodation" id="alert_accommodation" type="text" size="50" value="{{ $arr_settings['alert_sleeping'] }}"></td></tr>
	</table>
</div>
<div class="row">
	<table class="form-group">
		<tr><td>Nascondi blocco PERNOTTO E CAMERE <input name="hide_accommodation" id="hide_accommodation" type="checkbox" @if(!empty($arr_settings['hide_sleeping'])) checked @endif></td></tr>
	</table>
</div>
<div class="row">
	<button type="submit" id="save-event-settings-button" name="save-event-settings-button" class="btn btn-primary">Salva le impostazioni</button>
</div>
<br>
<div class="row">
	<table class="form-group">
	 <tr>
      <td style="padding-right:5px"><button id="delete-rp-from-date-button" name="delete-rp-from-date-button" class="btn btn-primary">Elimina registrati dalla data:</button></td>
      <td><input type="date" name="delete-rp-from-date" id="delete-rp-from-date"  value=""></td>
     </tr>
	</table>
</div>
<br>
<div class="row">
<h2><a href="{{ route('event_admin', $event->id) }}">Indietro</a></h2>
</div>
</section>
@endsection

@section('js_footer')
<script type="text/javascript">
$(document).ready(function () {
    
    $('#save-event-settings-button').click(function () {
		let hide_accommodation_action = 0;
		if($('#hide_accommodation').is(':checked')){
			hide_accommodation_action = 1;
		}
        $.ajax({
            url:"{{route('ajax_save_event_settings')}}",
            type:"POST",
            headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
			data:{
				event_id:{{ $event->id }}, 
				@php $k = 1; @endphp
				@foreach($arr_settings['events'] as $evento)
					event_label_{{$k}}:$('#event_label_{{$k}}').val(),
					event_title_{{$k}}:$('#event_title_{{$k}}').val(),
					event_date_start_{{$k}}:$('#event_date_start_{{$k}}').val(),
					event_alert_{{$k}}:$('#event_alert_{{$k}}').val(),
				@php $k++; @endphp
				@endforeach
				alert_accommodation:$('#alert_accommodation').val(),
				hide_accommodation:hide_accommodation_action
			},
            success:function(response) {
                alert(response);
            }
        });
    });
	
	$('#delete-rp-from-date-button').click(function () {
		if(!$('#delete-rp-from-date').val()){
			alert('Inserisci una data per favore');
			return false;
		}
		if(confirm('Vuoi veramente cancellare tutti gli utenti registrati dal '+$('#delete-rp-from-date').val()+' per questo evento?')){
			$.ajax({
				url:"{{route('ajax_delete_rp_from_date')}}",
				type:"POST",
				headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
				data:{
					event_id:{{ $event->id }}, 
					from_date:$('#delete-rp-from-date').val()
				},
				success:function(response) {
					alert(response);
				}
			});
		} else {
			alert('Cancellazione utenti abortita');
		}
    });
	
});
</script>
@endsection
