<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu tree">
            <!--li class="header">HEADER</li-->
            <!-- Optionally, you can add icons to the links -->
            <li class="treeview active">
                <a href="{{ route('home') }}"><span>Lista Eventi</span></a>
                <ul class="treeview-menu">
                    @foreach($Objevents as $single_event)
                    @if(!empty($event) && ($event->id == $single_event->id))
                    <li class="active">
                    @else
                    <li>
                    @endif
                     <a href="{{ route('event_admin', $single_event->id) }}" style="font-size:12px">{{ str_limit($single_event->name, $limit=32, $end="...") }}<br>&nbsp;{{ $single_event->description}}</a>
                    </li>
                    @endforeach
                </ul>
            </li>
            <!--li class="treeview active">
                <a href="#"><span>New Event</span></a>
            </li-->
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
