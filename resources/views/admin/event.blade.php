@extends('admin.partials.admin_template')

@section('content')

<section class="content">
      <div class="row">
       <div>
        @if($user->id <= 2)
			@if($event->status == 0)
		        <button class="btn btn-info btn-warning" id="hide-show-from-sidebar">Rimuovi dalla lista per i Guest</button>
			@elseif($event->status == -1)
		        <button class="btn btn-info btn-success" id="hide-show-from-sidebar">Aggiungi alla lista per i Guest</button>
			@endif
		@endif
        @if($event->status > 0)
        <button class="btn btn-info btn-danger" id="enable-disable-button">
        Disattiva
        @else
        <button class="btn btn-info btn-success" id="enable-disable-button"> 
        Attiva
        @endif
        modulo
        </button>
		<a href="{{ route('event_settings_admin', $event->id) }}">Impostazioni</a>&nbsp;&nbsp;<a href="{{route('event_show')}}/{{$event->id}}?preview=on">Visualizza preview modulo</a>
       </div>
      </div>
      </br>
      <div class="row">
        <strong>Utenti Registrati:&nbsp;</strong>{{$num_registered_people}}
        </br>
        </br>
        @if($num_registered_people > 0)
        <button id="export_csv" class="btn btn-block btn-primary">GENERA REPORT DA SCARICARE</button>
        <a id="download_registered_people" style="display:none" class="btn btn-block btn-primary">SCARICA REPORT</a>
        @endif
      </div>
	  <br>
      <div class="row">
        @if($num_registered_people > 0)
        <button id="view_csv" class="btn btn-block btn-primary">VISUALIZZA REGISTRATI</button>
		@endif
	  </div>
	  <div class="modal fade" id="registeredModal" tabindex="-1" role="dialog" aria-labelledby="registeredLabel">
		<div class="modal-body" style="overflow-x:auto;overflow-y:auto;background:white" id="modal-body">
		</div>
	  </div>
</section>
@endsection

@section('js_footer')
<script type="text/javascript">
$(document).ready(function () {
    $('#export_csv').click(function () {
        $.ajax({
            url:"{{route('ajax_get_csv')}}",
            type:"POST",
            dataType: 'text',
            headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
            data:{ event_id:{{ $event->id }} },
            success:function(response) {
                 csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(response);
                 $("#download_registered_people").attr({
                                                     "href": csvData,
                                                     "download": "sag_data.csv",
                                                     "style": "display:block"
                 });
                 $("#export_csv").attr("style", "display:none");
            }
        });
    });

	$('#view_csv').click(function() {
        $.ajax({
        	url:"{{route('ajax_view_csv')}}",
            type:"POST",
            dataType: 'text',
            headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
            data:{ event_id:{{ $event->id }} },
            success:function(response) {
				$('#modal-body').html(response);
				$('#registeredModal').modal('show');
			}
		});
	});
    
    $('#enable-disable-button').click(function () {
        $.ajax({
            url:"{{route('ajax_enable_disable_event')}}",
            type:"POST",
            headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
            data:{event_id:{{ $event->id }}, actual_event_status: {{ $event->status }}},
            success:function(response) {
                alert(response);
                location.reload();
            }
        });
    });
	
	$('#hide-show-from-sidebar').click(function () {
        $.ajax({
            url:"{{route('ajax_hide_show_from_sidebar_event')}}",
            type:"POST",
            headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
            data:{event_id:{{ $event->id }}, actual_event_status: {{ $event->status }}},
            success:function(response) {
                alert(response);
                location.reload();
            }
        });
    });
});
</script>
@endsection
